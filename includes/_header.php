<header itemscope itemtype="http://schema.org/Organization">
    
    <div class="container header-container-main">
        <div class="menu-list">
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <nav class="menu">
                <ul class="menu-list-2">
                    <?php if("http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] == $url){ ?>
                    <li><a href="javascript:void(0)" data-id="empresa" title="Empresa">Quem Somos</a></li>
                    <li><a href="javascript:void(0)" data-id="nossos-servicos" title="Serviços">Serviços</a></li>

                    <?php }else{ ?>
                    <li><a href="<?php echo $url; ?>#empresa" title="Empresa">Quem Somos</a></li>
                    <li><a href="<?php echo $url; ?>#nossos-servicos" title="Serviços">Serviços</a></li>
                     <?php } ?>
                </ul>
            </nav>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <div class="logo">
                <a href="<?php echo $url; ?>" title="<?php echo $h1 . " - " . $nome_empresa; ?>">
                    <span itemprop="image">
                        <img src="<?php echo $url; ?>imagens/logo.png" alt="<?php echo $nome_empresa; ?>" title="<?php echo $nome_empresa; ?>" class="img-responsive">
                    </span>
                </a>
            </div>
        </div>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
            <nav class="menu">
                <ul class="menu-list-2">
                    <?php if("http://".$_SERVER["HTTP_HOST"].$_SERVER["REQUEST_URI"] == $url){ ?>
                    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                    <li><a href="javascript:void(0)" data-id="contato" title="Contato">Contato</a></li>

                    <?php }else{ ?>
                    <li><a href="<?php echo $url; ?>informacoes" title="Informações">Informações</a></li>
                    <li><a href="<?php echo $url; ?>#contato" title="Contato">Contato</a></li>
                    <?php } ?>
                </ul>
            </nav>
        </div>
        </div>
    </div>
</header>