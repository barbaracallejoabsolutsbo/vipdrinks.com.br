<footer>
    <?php include "includes/btn-fixos.php"; ?>
    <div class="container">
        <div class="row">
            <div class="mapa-site">
                <a href="mapa-site.html" title="Mapa Do Site">Mapa do Site</a>
            </div>
        </div>
    </div>
        <ul class="menu-footer-mobile">
            <li><a href="tel:<?php echo $unidades[1]["ddd"].$unidades[1]["telefone"]; ?>" class="mm-call" title="Ligue"><i class="fas fa-phone-alt"></i></a></li>
            <li><a title="whatsApp" href="<?php echo $unidades[1]["whatsapp-link"]; ?>"><i class="fab fa-whatsapp"></i></a></li>
            <li><a href="mailto:<?php echo $emailContato; ?>" class="mm-email" title="E-mail"><i class="fas fa-envelope-open-text"></i></a></li>
            <li><button type="button" class="mm-up-to-top" title="Volte ao Topo"><i class="fas fa-arrow-up"></i></button></li>
        </ul>
    </footer>
    <?php if($_SERVER["SERVER_NAME"] != "clientes" && $_SERVER["SERVER_NAME"] != "localhost"){ ?>
        <!-- Código do Analytics aqui! -->
        <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-DY3FMH0EWF"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-DY3FMH0EWF');
</script>
        <?php } ?>