<?php

       // Principais Dados do Cliente
    $nome_empresa = "Vip Drinks";
    $emailContato = "vipdrinkseventos@gmail.com";

    // Parâmetros de Unidade
    $unidades = array(
    1 => array(
        "nome" => "Vip Drinks",
        "rua" => "R. Mozart, 62",
        "bairro" => "Vila Rosalia",
        "cidade" => "Guarulhos",
        "estado" => "São Paulo",
        "uf" => "SP",
        "cep" => "07072-040",
        "latitude_longitude" => "-23.450599687321958, -46.559702017907384", // Consultar no maps.google.com
        "ddd" => "11",
        "telefone" => "95388-4482",
        "whatsapp" => "95388-4482",
        "whatsapp-link" => "https://api.whatsapp.com/send?phone=551195388-4482&text=VipDrinks",
        "link_maps" => "" // Incorporar link do maps.google.com
    ),
        2 => array(
            "nome" => "",
            "rua" => "",
            "bairro" => "",
            "cidade" => "",
            "estado" => "",
            "uf" => "",
            "cep" => "",
            "ddd" => "",
            "telefone" => ""
        )
    );
    
    // Parâmetros para URL
    $padrao = new classPadrao(array(
        // URL local
        "http://localhost/vipdrinks.com.br/",
        // URL online
        "https://www.vipdrinks.com.br/"
    ));
    
    // Variáveis da head.php
    $url = $padrao->url;
    $canonical = $padrao->canonical;
    
    // Parâmetros para Formulário de Contato
    // $smtp_contato            = ""; // Solicitar ao líder do dpto técnico, 177.85.98.119
    // $email_remetente         = ""; // Criar no painel de hospedagem, admin@...
    // $senha_remetente         = "c0B1S3vH5eCvAO";

    // Contato Genérico (para sites que não se hospedam os e-mails)
     $smtp_contato            = "162.241.2.49";
     $email_remetente         = "dispara-email@absolutsbo.com.br";
     $senha_remetente         = "DisparaAbsolut123?";

    // Recaptcha Google
    $captcha                 = false; // https://www.google.com/recaptcha/
    $captcha_key_client_side = "";
    $captcha_key_server_side = "";

    // CSS default
    $padrao->css_files_default = array(
        "default/reset",
        "default/grid-system",
        "default/main",
        "default/slicknav-menu",
        "_main-style"
    );
    
    // JS Default
    $padrao->js_files_default = array(
        "default/jquery-1.9.1.min",
        "default/modernizr",
        "default/jquery.slicknav.min",
        "jquery.padrao.main"
    );
        
    // Listas de Palavras Chave
    $palavras_chave = array(
      "Aluguel de Carro de Noivas em Guarulhos",
      "Aluguel de Carro de Noivas em São Paulo",
      "Aluguel de Carro de Noivas no Morumbi",
      "Aluguel de Carro para Aniversário em SP",
      "Aluguel de Carro para Casamento",
      "Aluguel de Carro para Casamento em São Paulo",
      "Aluguel de Carro para Noiva",
      "Aluguel de Carro para Noivas em SP",
      "Aluguel de Carros de Noivas Preço",
      "Aluguel de Carros para Casamento",
      "Aluguel de Carros para Casamento em Guarulhos",
      "Aluguel de Carros para Casamento na Praia",
      "Aluguel de Carros para Debutantes",
      "Aluguel de Carros para Eventos",
      "Aluguel de Carros para Noivas",
      "Aluguel de Cascata de Chocolate",
      "Aluguel de Cascata de Chocolate em Arujá",
      "Aluguel de Cascata de Chocolate em SP",
      "Aluguel de Cascata de Chocolate no Morumbi",
      "Aluguel de Limusine para Casamento",
      "Aluguel de Limusine para Noiva",
      "Bar de Caipirinha em Casamento",
      "Bar de Caipirinha para Aniversário",
      "Bar de Caipirinha para Debutante",
      "Bar de Caipirinha para Evento",
      "Bar de Caipirinhas",
      "Bar de Coquetéis para Casamento",
      "Bar de Coquetéis para Casamento Simples",
      "Bar de Coquetéis para Festa",
      "Bar de Coquetéis para Festa Grande",
      "Bar de Coquetéis para Formatura",
      "Bar para Evento",
      "Bar para Evento Corporativo",
      "Bar para Evento Empresarial",
      "Bares para Eventos",
      "Bartender com Coquetel",
      "Bartender com Drinks",
      "Bartender para Debutante",
      "Bartender para Drinks sem álcool",
      "Bartender para Feira",
      "Bartender para Festas",
      "Bartender para Formatura",
      "Buffet de Coquetel",
      "Buffet de Coquetel a Domicílio",
      "Buffet de Coquetel para Aniversário",
      "Buffet de Coquetel para Debutante",
      "Buffet de Coquetel para Evento",
      "Buffet de Coquetel para Evento Corporativo",
      "Carro Casamento em São Paulo",
      "Carro para Eventos de Casamento",
      "Cascata de Chocolate em São Paulo",
      "Cascata de Chocolate para Aniversário",
      "Cascata de Chocolate para Casamento",
      "Cascata de Chocolate para Festa de Debutantes",
      "Cascata de Chocolate para Locação",
      "Cascata de Chocolate Preço",
      "Empresa de Barman em Arujá",
      "Empresa de Barman em Guarulhos",
      "Empresa de Barman em São Paulo",
      "Empresa de Barman no Morumbi",
      "Empresa de Eventos em São Paulo",
      "Empresa de Fondue em São Paulo",
      "Fondue para Aniversário de 15 Anos",
      "Fondue para Festa de Casamento",
      "Fondue para Festa de Casamento em Guarulhos",
      "Fondue para Festa de Casamento em São Paulo",
      "Fondue para Festa de Debutante",
      "Fondue para Festa de Debutante em Arujá",
      "Fondue para Festa de Debutante em São Paulo",
      "Fondue para Festa de Debutante no Morumbi",
      "Fondue para Festa em São Paulo",
      "Locação de Carro de Noivas Litoral",
      "Locação de Carro para Casamento",
      "Locação de Carro para Noiva",
      "Locação de Carro para Noiva em São Paulo",
      "Locação de Carros Noivas em Arujá",
      "Locação de Carros Noivas em Campinas",
      "Locação de Cascata de Chocolate",
      "Locação de Cascata de Chocolate em Arujá",
      "Locação de Cascata de Chocolate em Atibaia",
      "Locação de Cascata de Chocolate em SP",
      "Locação de Cascata de Chocolate no Morumbi",
      "Open Bar de Caipirinha",
      "Open Bar de Coquetel",
      "Open Bar para Festa",
      "Open Bar para Festa de 15 Anos",
      "Open Bar para Festa de Formatura",
      "Serviço de Bar em Guarulhos",
      "Serviço de Bar em São Paulo",
      "Serviço de Bar para Aniversário",
      "Serviço de Barman",
      "Serviço de Barman em São Paulo",
      "Serviço de Bartender para Evento",
      "Serviço de Bartender para Feira",
      "Serviço de Bartender para Festa de 15 Anos",
      "Serviço de Bartender para Formatura",
      "Serviço de Bartenders",
      "Serviços de Open Bar",
      "Serviços de Open Bar em São Paulo",
      "Serviços de Open Bar em Arujá",
);
   
    $palavras_chave_com_descricao = array(
        "Item 1" => "Lorem ipsum dolor sit amet.",
        "Item 2" => "Laudem dissentiunt ut per.",
        "Item 3" => "Solum repudiare dissentiunt at qui.",
        "Item 4" => "His at nobis placerat.",
        "Item 5" => "Ei justo lucilius nominati vim."
    );
    
     /**
     * Submenu
     * 
     * $opcoes = array(
     * "id" => "",
     * "class" => "",
     * "limit" => 9999,
     * "random" => false
     * );
     * 
     * $padrao->subMenu($palavras_chave, $opcoes);
     * 
     */

    /**
     * Breadcrumb
     * 
     * -> Propriedades
     * 
     * Altera a url da Home no breadcrumb
     * $padrao->breadcrumb_url_home = "";
     * 
     * Altera o texto que antecede a Home
     * $padrao->breadcrumb_text_before_home = "";
     * 
     * Altera o texto da Home no breadcrumb
     * $padrao->breadcrumb_text_home = "Home";
     * 
     * Altera o divisor de níveis do breadcrumb
     * $padrao->breadcrumb_spacer = " » ";
     * 
     * -> Função
     * 
     * Cria o breadcrumb
     * $padrao->breadcrumb(array("Informações", $h1));
     * 
     */

    /**
     * Lista Thumbs
     * 
     * $opcoes = array(
     * "id" => "",
     * "class_div" => "col-md-3",
     * "class_section" => "",
     * "class_img" => "img-responsive",
     * "title_tag" => "h2",
     * "folder_img" => "imagens/thumbs/",
     * "extension" => "jpg",
     * "limit" => 9999,
     * "type" => 1,
     * "random" => false,
     * "text" => "",
     * "headline_text" => "Veja Mais"
     * );
     * 
     * $padrao->listaThumbs($palavras_chave, $opcoes);
     * 
     */
    
    /**
     * Funções Extras
     * 
     * $padrao->formatStringToURL();
     * Reescreve um texto em uma URL válida
     * 
     */