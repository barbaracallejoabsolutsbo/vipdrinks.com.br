<?php
    $title       = "Cascata de Chocolate em São Paulo";
    $description = "A cascata de chocolate em São Paulo da Vipdrinks, com certeza será um diferencial em seu evento. Portanto, não deixe de garantir a mesma. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Vipdrinks possui a melhor cascata de chocolate em São Paulo, portanto você que está em busca pela mesma, encontrou o lugar ideal. Seja para eventos como, casamentos, aniversários, confraternização de empresas ou até mesmo corporativos, nós levaremos nossa cascata de chocolate em São Paulo, ao mesmo. Nós estamos absorvendo conhecimentos e experiências há longos anos, para que hoje utilizemos de técnicas totalmente exclusivas em nossa cascata de chocolate em São Paulo, para que sejamos referência em qualidade, nesse e demais serviços que disponibilizamos em nossa empresa. Nossos profissionais acompanham todo o processo com você, até que nossa cascata de chocolate em São Paulo, esteja em seu evento. Traga-nos suas ideias de frutas, doces e salgados que acompanharão nossa cascata, para tornarmos real o quanto antes, seu desejo. Vale lembrar que independente da proporção de seu evento, a nossa cascata de chocolate será apta aos seu evento, surpreendendo aos seus convidados e até mesmo a você, com a qualidade da mesma. Temos uma grande expansão em nossos serviços, para que você garanta o que mais se adapte a você. Portanto, navegue em nosso site para que você possa conhecer nossos trabalhos, além desse. Trabalhamos a cada dia, para que através dos recursos utilizados em nossos serviços, todas as suas necessidades sejam correspondidas. Portanto, não hesite em nos consultar, pois nós utilizaremos de nossos melhores métodos para colocarmos em prática, tudo o que é idealizado em sua mente. Sabemos da importância de levar satisfação e conforto aos nossos clientes e aos convidados de seus eventos e por isso possuímos os melhores alimentos, como chocolate e frutas, para que os mesmos tenham experiência conosco em todos os quesitos.</p>

<h2>Mais detalhes sobre nossa cascata de chocolate em São Paulo</h2>
<p>Caso você deseja obter nossa cascata de chocolate em São Paulo, juntamente com outro serviço que temos disponíveis, é só entrar em contato com um de nossos profissionais, onde os mesmos farão um atendimento personalizado, para que o seu dia e evento sejam inesquecíveis. Entre em nosso site e faça seu orçamento, até mesmo sem compromisso, para te retornarmos e colocarmos seus planos em prática o quanto antes.</p>

<h3>O melhor lugar para adquirir uma cascata de chocolate em São Paulo</h3>
<p>Estamos sempre inovando em nossa cascata de chocolate em São Paulo, portanto entre em contato com nossos profissionais para que os mesmos possam tirar qualquer dúvida que possuir perante aos nossos produtos/serviços.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>