<?php
    $title       = "Bar de Coquetéis para Formatura";
    $description = "A VIP Drinks oferece o serviço de bar de coquetéis para formatura. Monte um cardápio com opções alcoólicas e não alcoólicas para agradar todos os convidados.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>A VIP Drinks oferece serviço de bar e preparação de drinks para eventos variados, desde festa de debutante até casamentos, aniversários, confraternização, workshops, entre outros eventos, disponibilizamos atrações variadas para sua comemoração!</p>
<p>Os bares são atrações importantes nas festas, já que promovem movimento e diversão durante todo o evento. Os convidados se sentem a vontade para beberem o que desejar e o serviço de buffet e garçom fica menos sobrecarregado. Em eventos de formatura esse ponto é essencial, já que o número de convidados é grande e são diferentes os gostos e paladares.</p>
<h2>Conheça o bar de coquetéis para formatura</h2>
<p>Disponibilizamos uma carta de drinks com inúmeras variações, desde as mais clássicas até as mais modernas, que serão apropriadas para cada público de um determinado evento. A organização de uma formatura passa por diversos processos, e uma das etapas mais importantes é o serviço de bebidas. </p>
<p>Nosso bar de coquetéis para formatura conta com coquetelaria moderna e constantemente atualizada, para que nosso serviço seja cada vez mais aprimorado em prol de qualidade e satisfação de nossos clientes. </p>
<p>Por isso, o bar de coquetéis para formatura inova o ambiente e proporciona entretenimento durante toda a festa. Nossa equipe de bar é totalmente treinada e profissional para preparar seu drink de forma habilidosa e divertida. Assim, até no momento de aguardo, o bar de coquetéis para formatura oferece entretenimento e interação entre convidados e profissionais.</p>
<p>Ainda, nossos barmen e bargirls estão preparados para tirar qualquer dúvida sobre drinks, composição de bebidas e combinações adequadas ao seu paladar.</p>
<h3>Vantagens de um bar de coquetéis para formatura</h3>
<p>O bar de coquetéis para formatura é ideal para quem busca praticidade, conforto e bebidas de qualidade durante todo o evento. Feitos com bebidas de qualidade e ingredientes confiáveis, além do sabor, colocamos ênfase na apresentação dos drinks. </p>
<p>Portanto, o bar de coquetéis para formatura é grande atrativo durante toda a festa, entregando bebidas saborosas e sofisticadas. Oferecemos variedade e decoração apropriada, com temática específica para que o local seja um ponto de atração. Geralmente estar mais perto da pista de dança, atrai mais pessoas e proporciona uma combinação perfeita. </p>
<p>E mais que isso, o bar de coquetéis para formatura é um lugar de encontro para conversas e divertimento enquanto as bebidas são preparadas, deixando o espaço mais dinâmico. Para saber mais acesse nosso site, entregamos qualidade e diversão para que seu evento seja inesquecível e saboroso.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>