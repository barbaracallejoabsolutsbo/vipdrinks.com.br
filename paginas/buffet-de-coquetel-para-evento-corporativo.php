<?php
    $title       = "Buffet de Coquetel para Evento Corporativo";
    $description = "Obtenha nosso buffet de coquetel para evento corporativo. Os profissionais da Vipdrinks estão disponíveis para que você entre em contato e tirar suas dúvidas";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que deseja obter um buffet de coquetel para evento corporativo, a Vipdrinks tem os melhores recursos para que todos os convidados de seu evento possam ter todos os seus pedidos concretizados, fazendo com que mais uma parte dele os surpreenda. Ao longo de todos os nossos anos atuando nesse segmento, crescemos cada vez mais, consequentemente fazendo com que mais pessoas possam usufruir da qualidade de nosso buffet de coquetel para evento corporativo. Contamos com profissionais extremamente qualificados, que possuem inúmeras experiências não só em buffet de coquetel para evento corporativos, mas em todos os serviços que disponibilizamos. Em nosso site você poderá ver que atendemos em eventos corporativos e em eventos como casamentos, aniversários, confraternização de empresas e etc. Portanto, a Vipdrinks pode estar presente e te ajudar a realizar tal desejo, em qualquer evento que você nos solicitar. Estudamos todos os dias, para estarmos sempre utilizando de métodos novos e exclusivos, para que nos adaptemos a qualquer pedido de nossos clientes. Portanto, traga-nos suas ideias, para que possamos idealizar tudo o que você imagina perante ao nosso buffet de coquetel para evento corporativo. Nós fazemos a questão de desde seu primeiro contato conosco, nós mantermos contato com você, para que nossos projetos se alinhem e então, além de atendermos suas expectativas, superarmos as mesmas. Também, temos a ética profissional de fornecermos nossos melhores atendimentos para que sempre que você precisar de qualquer serviço que disponibilizamos, você já saiba que somos o lugar onde suas necessidades serão correspondidas.  </p>

<h2>Conheça mais sobre nosso buffet de coquetel para evento corporativo  </h2>
<p>Utilizamos sempre os melhores métodos em nosso buffet de coquetel para evento corporativo, para que atendemos a qualquer pedido que forem solicitados por você e por seus convidados. Desejamos que todos os que nos recorram, possam obter nossos serviços, no momento em que desejarem, E para isso, nós deixamos os preços de nossos serviços acessíveis para que os mesmos não tenham nenhum prejuízo e sim vantagens ao nos consultar.   </p>

<h3>A melhor opção de buffet de coquetel para evento corporativo  </h3>
<p>Temos uma grande diversidade em nossos serviços, para que você obtenha o que mais se adapte a você. Seja nosso buffet de coquetel para evento corporativo, ou demais serviços, nossos profissionais estão sempre disponíveis para te auxiliarem no que for necessário, na hora de sua escolha. Conte conosco para que façamos parte desse momento tão especial para você e seus convidados.  </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>