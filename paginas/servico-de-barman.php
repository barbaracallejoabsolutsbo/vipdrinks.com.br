<?php
    $title       = "Serviço de Barman";
    $description = "Serviço de Barman pode ser contratado com a VIP Drinks. Contamos com uma equipe profissional e especializada em eventos de dimensões variadas.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O planejamento de uma comemoração sempre é algo trabalhoso e exige atenção e dedicação, seja para uma festa de aniversário, de debutante, casamento, confraternização de empresa, feira de eventos, e assim por diante.</p>
<p>Um dos atrativos mais importantes para satisfazer os convidados e manter o evento agradável são as comidas e bebidas oferecidas. Para cada modelo de comemoração é fundamental a contratação de um serviço especializado. </p>
<p>O serviço de Barman tem essa proposta: tornar seu evento mais agradável e organizado. </p>
<h2>Benefícios na contratação do serviço de Barman</h2>
<p>O barman é um profissional especializado na preparação de drinks. Uma das principais vantagens em contratar um serviço de Barman é a oportunidade de apresentar aos seus convidados, bebidas exclusivas e diferenciadas. </p>
<p>Para que todos tenham opções variadas além das oferecidas em buffets, o cardápio no serviço de Barman é personalizado de acordo com cada cliente. Assim, os bartenders criam modelos exclusivos de bebidas de acordo com a preferência de cada um. </p>
<p>O atendimento animado é outro benefício na contratação de um serviço de Barman. Mais que um bar, ele funciona como atração na festa. O preparo de bebidas e como são feitas atrai convidados e torna o ambiente mais divertido e agitado, sem perder o profissionalismo. </p>
<p>As técnicas na preparação de bebidas são um verdadeiro show, garantindo uma experiência especial para todos. O serviço de Barman com o conhecimento de bebidas, ingredientes e combinações facilita a preparação de coquetéis e drinks, tornando o atendimento mais ágil e eficiente. Além disso, com a experiência em eventos variados, a organização dos itens e da qualidade dos produtos é processo garantido.</p>
<h3>Conheça a VIP Drinks e todos os nossos serviços</h3>
<p>Contamos com uma equipe profissional e especializada em eventos de dimensões variadas. O profissional no serviço de Barman atua com amplo conhecimento de bebidas, promovendo atenção especial para seus convidados, tornando seu evento ainda mais agradável. </p>
<p>Trabalhamos como uma equipe comprometida e garantimos o serviço de Barman de qualidade. Utilizamos as melhores bebidas, com marcas confiáveis, além de ingredientes bem selecionados. Tudo para que a satisfação de clientes e convidados seja atingida. </p>
<p>Estamos cientes da importância que seu evento carrega e da responsabilidade em servir bem a todos que chegam até nós. Conheça mais sobre nossos serviços, entre em contato conosco para solicitar um orçamento. Estamos dispostos a fazer parte da comemoração dos seus sonhos levando o melhor atendimento a você e aos seus convidados.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>