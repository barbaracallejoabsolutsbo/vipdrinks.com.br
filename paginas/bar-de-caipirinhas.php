<?php
    $title       = "Bar de Caipirinhas";
    $description = "A VIP Drinks oferece o bar de caipirinhas com equipe especializada para preparação de drinks. Oferecemos bebidas com e sem álcool, para agradar todos os gostos.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>Conheça nossas atrações de bares para casamentos, festas, workshops, e muito mais. Para um evento completo contrate o bar de caipirinhas organizado pela VIP Drinks. Somos uma empresa especializada em bares e entretenimento, levando os melhores drinks e coquetéis para sua festa!</p>
<h2>Conheça nosso bar de caipirinhas</h2>
<p>A caipirinha é uma bebida apreciada por diferentes grupos, conquistando paladares variados. Trabalhamos com diversas opções no bar de caipirinhas, montando variações de bebidas, frutas e especiarias que conquistarão seus convidados.</p>
<p>Nossas extensas opções formam o seu cardápio ideal, ou seja, serão apresentados aos seus convidados a elaboração de sabores e opções escolhidas por você. Isso facilita o processo na hora do pedido, além de permitir uma liberdade maior de escolha do que cada um irá consumir. </p>
<p>A interação com o barman é outro ponto importante. Nossos profissionais estão preparados para atendê-los da melhor maneira possível, proporcionando momentos de diversão e descontração enquanto o pedido é preparado. Atendemos com total compromisso e dedicação, contribuindo com respostas para possíveis dúvidas sobre drinks e composições das bebidas.</p>
<p>Os bares são atrações importantes nas festas, já que promovem movimento e diversão durante todo o evento. Os convidados se sentem à vontade para beberem o que desejar e o serviço de garçom fica menos sobrecarregado.</p>
<h3>Cardápio do bar de caipirinhas</h3>
<p>O bar de caipirinhas conta com bebidas de qualidade e marcas confiáveis, frutas que agradam desde paladares mais simples até os mais sofisticados.  As frutas cítricas ou adocicadas e as especiarias darão um toque especial para seu drink! Mais que o sabor, no bar de caipirinhas nossos bartenders se preocupam com a apresentação do seu pedido. O visual pode ser o primeiro passo para chamar a atenção, fazendo-os se interessar pelo bar de caipirinhas.</p>
<p>No bar de caipirinhas são oferecidas bebidas com e sem álcool, para agradar todos os gostos e respeitar todas as idades. Colocamos a segurança dos clientes em primeiro lugar. </p>
<p>  Sendo assim, nossos bares contam com três principais funções: o sabor dos drinks, a diversidade de escolhas e o visual de cada bebida. São esses fatores que contribuirão para uma ótima experiência dos seus convidados.</p>
<p>Nosso serviço atende diversos segmentos, oferecendo excelência e responsabilidade a todo momento. Por isso, se você busca por serviços de bartender, seja para festa de debutante, casamentos, aniversários, confraternizações, workshops, entre outros eventos, conheça a VIP Drinks. Entregamos qualidade e diversão para que seu evento seja inesquecível. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>