<?php
    $title       = "Cascata de Chocolate Para Casamento";
    $description = "A cascata de chocolate para casamento da Vipdrinks, levará mais um renome ao mesmo;. Portanto, entre em contato conosco o quanto antes para adquirir a mesma.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Seja na praia ou em buffet, nossa cascata de chocolate para casamento, será ideal para você. Pois além de ser um diferencial, farão com que os seus convidados se satisfaçam em mais uma questão. Continue nos acompanhando, para ver que somos a melhor opção de cascata de chocolate para casamento. A Vipdrinks é especialistas em eventos há longos anos, onde em todos, grandes conhecimentos e técnicas foram atribuídas, para que hoje sejamos referência nesse ramo; possuindo também a melhor cascata de chocolate para casamento. Em nosso site, você poderá ver que nós disponibilizamos diversos serviços para variados eventos, como aniversários e confraternização de empresas. Além de nós disponibilizarmos nossa cascata de chocolate para casamento, nós fazemos também o aluguel de carro para noivas, acompanhando-as em todos os processos antes da chegada ao local e até mesmo após. Navegue em nosso site, para que você possa ver imagens de nossos profissionais atuando nos mais variados eventos. Os materiais e produtos utilizados em nossa cascata de chocolate são sempre dos mais atualizados e os melhores do mercado, para que entreguemos um serviço de qualidade, alcançando e até mesmo suprindo suas expectativas perante aos nossos serviços. Entre em contato o quanto antes com um de nossos profissionais, para que você nos apresente suas ideias e podermos materializar tudo o que você deseja. É um prazer para nós realizarmos sonhos, com os nossos trabalhos. Você pode personalizar a nossa cascata com os alimentos escolhidos por você, para que fique mais perto da forma que você idealizou. Consulte um de nossos profissionais para nos apresentar suas ideias!</p>

<h2>Conheça mais sobre nossa cascata de chocolate para casamento</h2>
<p>Não pense que a nossa cascata de chocolate para casamento não pode ser uma opção para você. Pois um de nossos princípios é fazermos com que todos os que nos consulte tenham acesso ao o que desejam para conosco. Mantemos sempre uma alta qualidade, com um baixo custo, para que nossos clientes não tenham nenhum prejuízo financeiro ao entrar em contato conosco.</p>

<h3>O melhor lugar para adquirir uma cascata de chocolate para casamento</h3>
<p>Será um prazer à Vipdrinks, levar nossa cascata de chocolate para casamento, a você e aos seus convidados. Conte sempre com os nossos serviços para que possamos fazer parte de momentos especiais de sua vida. Queremos que sempre que você pensar em serviços para eventos, a Vipdrinks seja sua primeira opção. Estamos disponíveis a qualquer momento.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>