<?php
    $title       = "Aluguel de Carros Para Eventos";
    $description = "A Vip Drinks também possui outros serviços, conheça mais acessando nosso site. Temos todos disponíveis a você, a qualquer momento que precisar. Não deixe de entrar em contato conosco. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />Viva a experiência de ter o seu próprio chofer com o aluguel de carros para eventos da Vip Drinks. Com excelência atendemos garantindo um evento inesquecível a partir de um motorista profissional e um carro com luxo e elegância. Conheça nosso serviço acessando nosso site e chegue no seu evento com presença e primor.<br />Chegar com estilo em nosso carro de aluguel de carros para eventos é um cenário incrível para celebrações glamorosas. Para gravações de cinema, produções, videoclipes, lançamentos de produtos, casamentos, aniversários e muito mais. <br />Além do motorista devidamente trajado, o serviço à disposição dos clientes, realizando o trajeto entre o salão de beleza, a igreja e o salão de festa, por exemplo, com a possibilidade de alteração previamente informada em casos de casamentos e debutantes, o serviço de aluguel de carros para eventos possui possibilidade de permanência do carro no local do evento para fotos e imagens pessoais dos contratantes.<br />Garantindo um evento incrível para o anfitrião, o aluguel de carros para eventos proporciona conforto, sofisticação e elegância para o passageiro que poderá desfilar em uma carruagem. O carro do modelo Opirus é um carro de luxo e alto padrão com design da linha S-Type do Jaguar. Belo e robusto, é um clássico modelo para casamentos e debutantes. Contrate agora mesmo os serviços da Vip Drinks.</p>
<h2><br />Aluguel de carros para eventos com antecedência e benefícios</h2>
<p><br />Efetuando seu aluguel de carros para eventos com pelo menos 20 dias de antecedência, o casal pode personalizar uma placa de brinde para utilizar no dia do evento e para posteriormente ao fim do serviço, ficar de presente para recordações e decoração. A Vip Drinks também possui outros serviços, conheça mais acessando nosso site.</p>
<h2><br />Serviços especiais com a Vip Drinks e o aluguel de carros para eventos</h2>
<p><br />Além de aluguel de carros para eventos, a Vip Drinks oferece serviços de bartender para open bar em festas de debutantes, casamentos, coquetéis de lançamento de lojas, confraternizações em geral e eventos empresariais. Cardápio de drinks alcoólicos e não alcoólicos personalizados, barman, bebidas e cascata de chocolate são alguns opcionais. Fale com a Vip Drinks e conheça mais sobre os serviços exclusivos para eventos.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>