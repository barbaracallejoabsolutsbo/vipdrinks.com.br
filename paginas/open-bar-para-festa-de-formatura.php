<?php
    $title       = "Open Bar para Festa de Formatura";
    $description = "Nosso open bar para festa de formatura fará com que seus convidados tenham boas memórias. Os serviços da Vipdrinks com certeza te surpreenderão. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A formatura é um momento único na vida de milhares de pessoas e por conta disso sabemos da importância que é fazer com que tudo saia extremamente perfeito nesse dia.  E para que isso ocorra, o nosso open bar para festa de formatura é extremamente essencial, para que você tenha incríveis memórias com o seus amigos e familiares. Nosso objetivo, é fazer com que todos os que usrufruirem de nosso open bar para festa de formatura, possam ter suas necessidades correspondidas. Portanto, nós possuímos uma variedade nossos drinks para que tal feito ocorra.  Os profissionais da vipdrinks possuem diversas experiências em open bar para festa de formatura e por isso estão aptos a qualquer situação que chegar até os mesmos. Fora que também possui experiências em serviços para eventos como casamento, aniversários, eventos corporativos, confraternização de empresas e entre outros. Queremos deixar claro que nós possuímos grandes conhecimentos e experiências para que possamos fazer com que o seu evento seja da forma como você e seus convidados desejam. Vale lembrar que seja para open bar para festa de formatura ou demais serviços para eventos, você pode contar com a vipdrinks. Pois temos todos os recursos necessários para tal. Contudo não hesite em entrar em contato com um de nossos profissionais para saber mais detalhadamente sobre. Desde o seu primeiro contato conosco, nós entregamos o melhor atendimento possível a você, para que sempre que precisar de qualquer serviço para qualquer evento, você saiba que a Vipdrinks é o único lugar onde todas as suas expectativas serão correspondidas da forma que você sempre desejou. </p>

<p>Mais informações sobre o nosso open bar para festa de formatura</p>
<p>Em nosso site você pode realizar o seu orçamento para o nosso open bar para festa de formatura E ver que esse nosso serviço está cada vez mais próximo de você e de seus convidados. Garantimos que com a vip tristes você terá a melhor experiência em todos os quesitos e com certeza vai querer experimentar dos mesmos mais de uma vez. </p>

<p>A melhor opção de open bar para festa de formatura</p>
<p>Um dos princípios da Vipdrinks É realizar meu sonhos através de nossos trabalhos. Entre em contato com de nossos profissionais para tirar qualquer dúvida que você possuir perante ao nosso open bar para festa de formatura ou até mesmo para garantir esse e demais serviços que disponibilizamos. Será um prazer levarmos nosso serviço a você.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>