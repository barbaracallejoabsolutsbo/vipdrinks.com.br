<?php
    $title       = "Buffet de Coquetel";
    $description = "Conte sempre com o serviço de buffet de coquetel da Vipdrinks para o seu evento. Será um prazer levarmos nossos drinks a você e aos seus convidados. Estamos sempre a disposição. Entre em contato conosco.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que sempre desejou possuir um evento com buffet de coquetel, que não te levará nenhum prejuízo financeiro, encontrou o lugar ideal. Ao navegar em nosso site, você verá que possuímos variados serviços para todos os tipos de eventos. Portanto, independente do evento de sua escolha e necessidade, nossos serviços são essenciais para o mesmo. Um dos primeiros pensamentos que surgem, à partir da ideia de realizar um evento, é o conforto dos convidados. Tanto para comida, quanto para bebida, mantendo a diversão. Portanto, nosso buffet de coquetel é totalmente necessário para este momento. Temos o objetivo de nos tornarmos referência nesse ramo e para isso, utilizamos técnicas únicas em nosso buffet de coquetel, para que sejamos exclusivos no quesito qualidade. Seja para casamentos, aniversários, confraternização de empresas e demais eventos, nosso buffet de coquetel pode ser utilizado também. Pois qualquer um de nossos serviços são adaptáveis e qualificados a qualquer tipo de evento. Além do nosso buffet satisfazer seus convidados, nossos drinks deixarão aquele gostinho de quero mais, fazendo com que seu evento sempre seja relembrado em suas memórias. Devido a qualidade da prestação de nossos serviços, a busca pelos mesmos estão crescendo dia após dia e ficamos extremamente felizes, pois sabemos que mais pessoas estão tendo acesso a experiência incrível que é usufruir de nossos trabalhos. Não adie ainda mais essa oportunidade de fazer com que mais uma parte de seu evento seja memorável e inesquecível. Conte com a Vipdrinks para fazermos parte desse momento especial, realizando o seu sonho.</p>

<h2>Conheça mais detalhes sobre nosso buffet de coquetel</h2>
<p>Vale lembrar que nosso buffet de coquetel é qualificado para qualquer tipo de evento, portanto não hesite em nos consultar. Traga-nos suas ideias para que possamos as colocar em prática o quanto antes, fazendo com que tudo saia da forma em que você idealizou. Os valores de nossos serviços são sempre acessíveis para que a qualquer momento que você necessitar de qualquer um de nossos serviços, você nos consulte e tenha experiências incríveis. Desde seu primeiro contato conosco, você terá o melhor atendimento, pois além de querermos que você usufrua de nossos serviços, priorizamos a forma em que você deve ser tratado, para garantirmos a sua confiança.</p>

<h2>A melhor opção de buffet de coquetel</h2>
<p>Em nosso buffet de coquetel, são utilizados dos melhores materiais e produtos, para que entreguemos um serviço com qualidade única, mantendo uma extrema excelência.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>