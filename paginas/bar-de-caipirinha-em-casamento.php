<?php
    $title       = "Bar de Caipirinha em Casamento";
    $description = "Nossos meios de contatos disponibilizados em nosso site, estão sempre disponíveis para que você possa entrar em contato com um de nossos especialistas e tirar qualquer dúvida que possuir. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que busca proporcionar momentos inesquecíveis para os convidados de sua festa de casamento, nosso bar de caipirinha em casamento é aonde tudo é feito da forma que você deseja, correspondendo sempre todas as suas necessidades e de seus convidados. A cada ano de nossa atuação nesse ramo e desde a nossa fundação, estamos sempre inovando, consequentemente trazendo novos métodos únicos nesse mercado, para que possamos nos tornar cada vez mais referência no ramo de bar de caipirinha em casamento e em serviços especializados em Open Bar, nos mais variados tipos de evento. Nós da Vipdrinks sempre nos adaptamos a todos os pedidos de nossos clientes, para que o resultado do nosso serviço de bar de caipirinha, seja além do que se é esperado. Garanta o quanto antes nossos serviços de bar de caipirinha em casamento, para que você possa ver em prática, tudo o que lhe apresentamos em teoria. Garantimos que você terá grandes experiências com a Vipdrinks, não só com o nosso bar de caipirinha, mas com todos os nossos produtos. Contudo, não adie cada vez mais essa oportunidade incrível e única.</p>
<h2>Mais informações sobre o nosso bar de caipirinha em casamento</h2>
<p><br />Como queremos que cada vez mais pessoas tenham acesso as experiências que podemos proporcionar com os nossos serviços, nós priorizamos fazer com que os valores dos mesmos sejam completamente acessíveis. Portanto, não deixe de fazer seu orçamento do nosso bar de caipirinha em casamento, através de uma de nossas redes sociais, com algum de nossos profissionais; ou até mesmo pelo nosso site, onde possui uma parte específica para que você preencha os espaços e nos envie-o. Nó analisamos e reanalisamos cada processo dos nossos drinks, para que cheguem em perfeito estado e condições em suas mãos. Nós possuímos diversos meios de contato para que você possa falar com um de nossos profissionais sobre nosso bar de caipirinha em casamento, a qualquer momento que desejar. </p>
<h2>O melhor lugar para adquirir um bar de caipirinha em casamento</h2>
<p><br />Como já citado, possuímos diversos meios de contato. E através deles, você pode consultar nossos especialistas para fazermos o seu orçamento, ou para tirarmos dúvidas específicas sobre o nosso bar de caipirinha em casamento, ou demais outros. Através de nossas redes sociais, como facebook e instagram, você acompanhará nossos serviços com mais detalhes, obtendo mais informações também e conhece mais sobre os nossos serviços que sempre estão disponíveis a você. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>