<?php
    $title       = "Empresa de Fondue em São Paulo";
    $description = "Empresa de fondue em São Paulo é com a VIP Drinks. Contamos com uma equipe profissional e especializada em eventos de dimensões variadas";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Seja para festas de aniversário, casamentos, formaturas, confraternizações corporativas ou qualquer outro evento, contratar uma boa empresa de eventos é fundamental para o sucesso de sua comemoração. Se você procura por um serviço de cascatas de chocolate, somos uma empresa de fondue em São Paulo. </p>
<p>Uma das maiores preocupações na organização de eventos são as comidas e bebidas. Nossa empresa de fondue em São Paulo tornará o ambiente de sua comemoração mais agradável e contribuirá positivamente para sua festa. </p>
<h2>Conheça nossos objetivos como empresa de fondue em São Paulo</h2>
<p>Oferecemos um serviço de cascata de chocolate para sua festa. Não tem nada melhor do que contar com uma empresa que ajude na organização de sua festa enquanto você se diverte e interage com os convidados. Mais que um atendimento, nossa empresa de fondue em São Paulo proporciona diversão e entretenimento a todos.</p>
<p>Nosso objetivo visa facilitar o planejamento do seu evento, satisfazer os convidados e levar diversão a todos. Contamos com uma equipe preparada para eventos de grande, média e pequena proporção. Nossa empresa de fondue em São Paulo torna seu evento ainda mais agradável, levando um momento incrível com todo o sabor até a sua festa.</p>
<h3>Saiba mais sobre nossa empresa de fondue em São Paulo</h3>
<p>Nossos produtos são escolhidos de forma profissional e inteligente. Nossa empresa de fondue em São Paulo trabalha com chocolate branco, ao leite ou até amargo que fará sucesso entre o público, seja ele adulto, adolescente ou infantil. Proporcionado o que há de melhor no mercado, as frutas que utilizamos são frescas e da melhor qualidade.</p>
<p>Somos responsáveis pela qualidade e contabilização dos produtos durante seu evento, garantindo um atendimento responsável. Esse recurso promete ótimo custo benefício aos nossos clientes, já que as perdas e excessos são controlados com a experiência que adquirimos ao longo do tempo.</p>
<p>Oferecemos uma variedade de opções de mesa decorada, doces, enfeites e temáticas para combinar com a proposta do evento. Assim, na contratação da empresa de fondue em São Paulo, a preparação para a sua festa é feita pensando em todos os gostos e paladares, indo além dos serviços ofertados por buffets tradicionais. </p>
<p>Estamos cientes da importância do seu evento e da responsabilidade em servir bem a todos que chegam até nós. Estamos aqui para fazer parte da comemoração dos seus sonhos. Conheça mais sobre nossos serviços e entre em contato com nossa equipe para solicitar um orçamento. </p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>