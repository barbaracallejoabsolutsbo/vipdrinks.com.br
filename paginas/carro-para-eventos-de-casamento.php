<?php
    $title       = "Carro Para Eventos de Casamento";
    $description = "O carro para eventos de casamento é um serviço oferecido pela VIP Carro Noivas. Somos uma empresa especializada em organização de eventos especiais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>No dia do seu casamento o que não pode faltar é o carro que te levará até o local de cerimônia. Durante o planejamento desse grande dia é importante escolher uma empresa confiável e profissional no aluguel de carro para eventos de casamento.</p>
<p>A VIP Carro Noivas conta com número limitado de carros exclusivos do modelo Opirus na cor preta. Elegante e raro, esse carro para eventos de casamento garante um trajeto tranquilo e uma entrada memorável.</p>
<h2>Aluguel de carro para eventos de casamento é com a VIP Carro Noivas</h2>
<p>Somos uma empresa especializada em organização de eventos especiais e prestamos serviço de aluguel de carro para eventos de casamento que inclui locomoção dos noivos e um serviço completo e atencioso durante todos os trajetos. </p>
<p>O que isso quer dizer? Nosso atendimento consiste em buscar a noiva — normalmente no cabeleireiro — levá-la até o local de cerimônia, levar o casal até o espaço de recepção ou festa e ainda, o transporte dos noivos até o local de núpcias depois da recepção. O carro para eventos de casamento permanece junto aos noivos por todo o tempo, incluindo o pós cerimônia e o momento de sessão de fotos do casal. </p>
<p>Como cortesia, oferecemos uma placa decorativa e personalizada, para que esse dia seja ainda mais especial e memorável. </p>
<h3>Conheça nosso carro para eventos de casamento</h3>
<p>A escolha do carro para eventos de casamento é fundamental ao ser o primeiro passo para a noiva chegar ao local de cerimônia. Por isso, garantir um veículo seguro, elegante e confortável é essencial. </p>
<p>Disponibilizamos carros de luxo de alto padrão na cor preta, totalmente digital, com ar-condicionado e teto solar. Todos esses detalhes contribuem para a perfeita chegada da noiva e um trajeto perfeito nesse dia tão especial.</p>
<p>Nosso carro para eventos de casamento possui excelente espaço traseiro para acomodação da noiva e possível acompanhante independentemente do vestido, proporcionando conforto sem prejudicar as vestimentas e o arranjo de cabelo. </p>
<p>Mais que o serviço de aluguel de carro, prestamos um serviço atencioso desde a saída do salão de beleza, até o final de sua comemoração. Colocamos segurança, profissionalismo e discrição para que seu sonho seja realizado. </p>
<p>Atendemos toda a região da cidade de São Paulo e nosso objetivo é proporcionar aos noivos um transporte seguro e com todo o conforto para um momento tão especial. Entre em contato com nossa equipe para mais informações.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>