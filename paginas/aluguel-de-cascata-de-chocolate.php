<?php
    $title       = "Aluguel de Cascata de Chocolate";
    $description = "Acessando o site da Vip Drinks, você encontra diferentes modelos de estruturas como essa e serviços especializados para eventos como festas, aniversários, casamentos e muito mais. Entre em contato com a nossa equipe, tire todas as suas dúvidas e realize u";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p><br />Conheça o aluguel de cascata de chocolate da Vip Drinks, próprio para eventos como aniversários, festas de debutante, casamentos, coquetéis, lançamento de lojas, entre outros. Com mão de obra inclusa, conheça nossos serviços e contrate a Vip Drinks para oferecer suporte ao seu evento.<br />Importante para o sucesso de sua festa, reservar com antecedência o aluguel de cascata de chocolate é importante para melhor escolher as frutas de época. A partir daí é possível montar uma mesa com frutas, guloseimas e decoração por conta do cliente que poderá ser utilizada para consumir com o chocolate utilizado na cascata.<br />O aluguel de cascata de chocolate é muito usado em festas, como casamentos, formaturas e aniversários de quinze anos. É um objeto onde o chocolate em forma líquida passa pelas diversas camadas, fazendo uma cascata deliciosa. As pessoas podem comer chocolate puro, mas na maioria dos casos ela é usada como um complemento para fondue, um tipo de sobremesa que mistura frutas ou aperitivos salgados com o chocolate derretido. <br />Além de uma opção saborosa, o aluguel de cascata de chocolate permite que o chocolate seja apreciado por muitas pessoas, tendo em vista que tem chocolate suficiente para diversos convidados, além de a cascata dar um toque especial na decoração da festa.</p>
<h2><br />O que combina para usar durante o aluguel de cascata de chocolate</h2>
<p><br />O aluguel de cascata de chocolate proporciona o cenário perfeito para consumir frutas com chocolate. Selecionam-se diversos tipos de frutas e itens para serem cobertos com a calda de chocolate como morango, banana, uva, kiwi, abacaxi, mexerica, waffle, marshmallow, biscoito champanhe, cookies, carolinas recheadas, banana-passa, damasco-seco, figo em compota, figo natural e mini-torrones, entre outras opções.</p>
<h2><br />Benefícios em contratar nosso aluguel de cascata de chocolate</h2>
<p>Entre os benefícios de optar por nosso aluguel de cascata de chocolate podemos destacar que garantimos assessoria de manuseio com os equipamentos alugados, também fornecemos uma lista de compras dos ingredientes necessários para sua cascata de chocolate ser um sucesso.<br />Acessando o site da Vip Drinks, você encontra diferentes modelos de estruturas como essa e serviços especializados para eventos como festas, aniversários, casamentos e muito mais. Entre em contato com a nossa equipe, tire todas as suas dúvidas e realize um orçamento sem compromisso.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>