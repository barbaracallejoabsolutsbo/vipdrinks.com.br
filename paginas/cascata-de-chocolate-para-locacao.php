<?php
    $title       = "Cascata de Chocolate Para Locação";
    $description = "A cascata de chocolate para locação da Vipdrinks será um diferencial em seu evento. Portanto, não perca a oportunidade de obtê-la no mesmo.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A Vipdrinks possui um histórico de longos anos atuando em eventos e fornecendo nossa cascata de chocolate para locação. Nós atendemos para os mais variados eventos, como casamentos, aniversários, confraternização de empresas, entre outros. Portanto, independente da proporção de seu evento e de qual for, nossa cascata de chocolate para locação, é ideal para o mesmo. Pois além de ser um diferencial em seu evento, levará extrema satisfação aos seus convidados. Com a nossa cascata de chocolate para locação, você terá uma amplitude de variedades de alimentos, para que possa atender a necessidade de todos em seu evento. Temos uma expansão em nossos serviços, para que além de consultar a nossa cascata de chocolate para locação, você possa consultar nossos outros trabalhos que estão disponóveis em nosso site, para que o seu dia seja ainda mais especial. Entre em contato com um de nossos especialistas, para nos trazer suas ideias e planos para o seu dia, para adaptarmos nossos trabalhos a ele. A Vipdrinks tem todos os recursos necessários para realizarmos todos os pedidos que chegam até nós. Portanto, não hesite ao nos consultar, pois é um prazer para nós, realizarmos sonhos com os nossos serviços. Todos os nossos profissionais possuem anos de experiência na entrega dos nossos serviços para eventos; e aos nos consultar você verá que somos o melhor lugar para se recorrer, para um dia tão importante. Sabemos da importância que é obter a sua confiança e para isso, nós fazemos a questão de te apresentarmos todas as nossas técnicas, materiais e alimentos utilizados em nossa cascata, moldando-os conforme for pedido por você, para que suas necessidades sejam correspondidas.</p>

<h2>Mais detalhes sobre nossa cascata de chocolate para locação</h2>
<p>Temos ciência da qualidade que há em nossa cascata de chocolate para locação e por isso, desejamos que a cada dia, mais pessoas possam ter acesso a mesma, para ter uma experiência incrível e memorável. E para isso, nós mantemos os nossos valores acessíveis e maleáveis, para que a qualquer momento que você nos consultar, automaticamente consiga obtê-la. Mantendo sempre um baixo custo e uma alta qualidade.</p>
<h3></h3>
<h3>A melhor opção de cascata de chocolate para locação</h3>
<p>Em nosso site, você pode realizar seu orçamento personalizado perante a nossa cascata de chocolate para locação, de forma rápida e sem sair de casa. Nossos profissionais estão sempre disponíveis para te atender e prestar todos os serviços que forem necessários.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>