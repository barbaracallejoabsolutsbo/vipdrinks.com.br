<?php
    $title       = "Open Bar de Caipirinha";
    $description = "odos os meios de contatos que estão disponibilizados em nosso site, estão disponíveis a todo momento para que você possa entrar em contato conosco da forma que desejar, até mesmo para tirar dúvidas mais específicas e detalhadas. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Obter um open bar de caipirinha, com certeza será um diferencial em seu evento, fazendo com que esse seja mais um motivo para que seus convidados tenham boas lembranças do mesmo. Independente da proporção de seu evento e seja ele qual for, o nosso open bar de caipirinha é uma de nossas melhores opções para diversão e entretenimento. Temos o objetivo de sempre realizarmos os desejos de nossos clientes, da forma como nos são solicitados e para isso, nos adaptamos a todos os pedidos para que além do nosso open bar de caipirinha, tudo ocorra como esperado. Vale lembrar que além de fornecer nosso serviço de open bar de caipirinha, nós fornecemos diversos outros em eventos como: casamento, confraternização de empresas, aniversários e demais outros. Contudo, há diversas possibilidades para que você possa usufruir de nossos serviços. Nossos profissionais são grandes especialistas nesse ramo, pois possuem longas experiências no mesmo e isso faz com que nós concretizemos tudo o que nos é solicitado, pois possuímos um amplo conhecimento. Nós sempre entregamos nosso melhor serviço e atendimento, para que sempre que você precisar de qualquer um de nossos trabalhos prestados, você nos recorra, pois saberá que somos o único lugar que pode corresponder as suas expectativas.</p>

<h2>Mais informações sobre o nosso open bar de caipirinha</h2>
<p>Todos os meios de contatos que estão disponibilizados em nosso site, estão disponíveis a todo momento para que você possa entrar em contato conosco da forma que desejar, até mesmo para tirar dúvidas mais específicas e detalhadas. Nossos profissionais designarão toda a atenção a você, para que o nosso open bar de caipirinha ocorra da forma como você deseja, em seu evento. Conte sempre com a Vipdrinks para fazer parte de qualquer evento que você desejar, levando sempre alegria, diversão e principalmente, realizando sonhos.</p>

<h2>A melhor opção para open bar de caipirinha</h2>
<p>Em nosso site há uma parte específica onde você pode realizar o seu orçamente, sem sair de casa. Priorizamos manter nossa alta qualidade e um baixo custo, para que todos tenham acesso a experiência do open bar de caipirinha. Garantimos que você não terá nenhum prejuízo ao nos consultar, pois somos profissionais qualificados para qualquer tipo de evento e situação. Será um prazer podermos levar nossos trabalhos a você e aos seus convidados. Abrace essa oportunidade única, para fazer com que o seu evento, seja seguido de somente, boas recordações. Nós aguardamos por você. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>