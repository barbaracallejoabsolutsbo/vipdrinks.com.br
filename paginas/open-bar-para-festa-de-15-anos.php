<?php
    $title       = "Open Bar para Festa de 15 Anos";
    $description = "O open bar para festa de 15 anos da Vipdrinks levará um diferencial em sua festa. Portanto, não perca essa oportunidade de garantir nossos serviços. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>A festa de debutante é um dia muito importante e especial para diversas pessoas. E para que você tenha ainda mais memórias incríveis nesse dia, obtenha o serviço de open bar para festa de 15 anos da Vipdrinks. Sera um imenso prazer para nós, levarmos esse e demais serviços até você e aos seus convidados. Nós entregamos um serviço com extrema qualidade e excelência, pois possuímos os melhores recursos para tal. Contudo, não adie mais essa oportunidade de conhecer mais sobre nosso open bar para festa de 15 anos. Queremos que tudo o que você idealize em sua mente, se concretize e para isso, fazemos a questão de mantermos nosso baixo custo, com alta qualidade. O Open bar para festa de 15 anos serve tanto para festas, obviamente, mas nós entregamos nosso serviços para demais eventos como: casamentos, confraternização de empresas, eventos corporativos e demais outros. Portanto, independente da proporção de seu eventos e para o que deseja, nossos serviços são essenciais para levaram conforto a você e aos seus convidados. Temos a certeza de que todos presentes em sua festa, terão experiências únicas e incríveis não só com os drinks de nosso open bar para festa de 15 anos, mas com o atendimento prestados pelos nossos funcionários. Desde seu primeiro contato conosco, você se torna nossa prioridade, pois queremos que seus sonhos sejam realizados através de nossos serviços, com todas as suas necessidades sendo correspondidas por nós. Lembrando que além desse nosso serviço nós possuímos diversos outros que podem ser adequados para que o seu dia seja ainda mais personalizado e especial para você. </p>

<p>Mais detalhes sobre nosso open bar para festa de 15 anos</p>
<p>Não perca oportunidade de obter o nosso open bar para festa de 15 anos e faça já o seu orçamento de forma on-line em nosso site. Mas caso prefira nossos profissionais estão sempre disponíveis para que você fale diretamente com um deles; até mesmo para tirar possíveis dúvidas perante a esse e demais outros serviços que possuímos na vip drinks. Será um prazer podemos fazer parte de um momento tão único para você. </p>

<p>O melhor open bar para festa de 15 anos</p>
<p>Nós temos todos os recursos necessários para nos adaptarmos a qualquer pedido perante ao nosso open bar para festa de 15 anos. Portanto nos traga sua ideia para que possamos fazer o seu atendimento de forma personalizada e realizarmos tudo o que você deseja. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>