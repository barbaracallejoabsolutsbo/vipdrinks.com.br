<?php
    $title       = "Bar para Evento";
    $description = "Você pode realizar o seu orçamento de forma rápida, consultando nosso site. Onde é disponibilizada uma parte específica para que você o faça de forma online. Todavia, possuímos nossos meios de contatos para que você possa falar diretamente com um de nosso";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para que o seu evento seja marcante em todos os quesitos, consulte nosso bar para eventos para que você possa surpreender seus convidados, fazendo com que ao lembrarem do seu evento, queiram voltar para tal momento pela qualidade do mesmo. Visamos sempre proporcionar grandes experiências, não só para aqueles que nos procuram, mas para todos os que de alguma forma, poderão usufruir de nosso bar para eventos. Nós realizamos serviços para diversos tipos de eventos, tal como aniversários, confraternização de empresas, festas de debutante, casamento e demais outros. Portanto, independente para o tipo do seu, não deixe de consultar o nosso bar, pois com certeza, corresponderemos as suas necessidades e atenderemos aos seus pedidos. A cada evento e a cada dia, nossos profissionais absorvem novos conhecimentos, para que possam aplicar no momento de atuação em nosso bar, levando cada vez mais um serviço qualificado. Desde o seu mínimo contato conosco, nós lhe entregaremos nosso melhor atendimento e serviço, para que ao precisar de um bar para eventos, você já saiba que com a Vipdrinks, você terá um atendimento único e exclusivo. </p>
<h2><br />Conheça mais sobre nosso bar para eventos</h2>
<p><br />Nós fazemos a questão de acompanhar cada processo de nossos drinks de perto, desde o momento da ideia de execução, para que possamos ir além de seus objetivos para com a nossa qualidade. E antes do evento, nós revisamos o cardápio dos drinks de nosso bar para eventos com você, para que você tenha tudo da forma como imaginou. Sabemos da qualidade que há em nossos serviços e produtos e para que mais pessoas tenham acesso aos mesmos, os valores de nosso bar para eventos, são extremamente acessíveis. Portanto, priorizamos sempre manter nossa alta qualidade, com um baixo custo. Você pode realizar o seu orçamento de forma rápida, consultando nosso site, onde é disponibilizada uma parte específica para que você o faça de forma online. Todavia, possuímos nossos meios de contatos que também estão disponíveis no site, para que você possa falar diretamente com um de nossos especialistas. </p>
<h2><br />A melhor opção para um bar para eventos</h2>
<p><br />Garantimos que conosco você terá a melhor experiência como cliente, pois nossa prioridade é sempre atender todos os pedidos que nos são solicitados. Portanto, não perca essa oportunidade de obter o melhor bar para eventos, em seu evento. Contamos com profissionais extremamente qualificados e que se adaptam a qualquer pedido e então, entregá-los com grande êxito.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>