<?php
    $title       = "Fondue Para Aniversário de 15 Anos";
    $description = "Garanta o fondue para aniversário de 15 anos da Vipdrinks, para que o seu dia seja especial da forma como você merece. Entre em contato conosco para obtê-lo. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que está a procura do melhor fondue para aniversário de 15 anos, encontrou o lugar ideal para tal. Nós estamos atuando nesse ramo há longos anos, entregando nosso fondue para aniversário de 15 anos e demais eventos como confraternização de empresas, casamentos e etec. Portanto, independente do tipo de evento desejado, nossos serviços serão aptos para você e para os seus convidados. Vale lembrar que além de fondue para aniversário de 15 anos, nós possuímos outros serviços que podem ser usufruídos nesse dia mais que especial. Navegue em nosso site para conhecer um pouco mais sobre eles e quem sabe, garanti-los. Em nosso site você poderá ver também imagens de nossos profissionais exercendo nossos serviços em eventos e também do nosso fondue para aniversário de 15 anos. Os mesmos possuem longos anos de experiência nesse ramo, onde em todos eles foram atribuídos novos conhecimentos, para que hoje aplicássemos na atuação de nossos trabalhos. Conhecimentos que geraram novas técnicas em nossos serviços, para que a cada dia aprimorássemos nossa qualidade, nos tornando cada vez mais referência para aqueles que já conhecem nossos serviços e para os que não conhecem também. Nós entregamos sempre nosso melhor atendimento, para que a qualquer momento que você necessitar desse nosso serviço, ou qualquer outro que temos disponíveis, você nos recorra, pelo fato de termos conquistado sua confiança; e para que você possa ter todas as suas necessidades correspondidas. Não se esqueça que temos todos os recursos necessários para qualquer evento, portanto não importa a proporção do mesmo, ou qual seja, qualquer um de nossos serviços levarão um diferencial para o mesmo.</p>

<h2>Conheça mais sobre nosso fondue para aniversário de 15 anos</h2>
<p>Sabemos o quanto esse dia é especial às debutantes e para que esse dia fique eternizado em sua memória de uma forma boa, o nosso fondue para aniversário de 15 anos é extremamente essencial. E para que todos tenham acesso ao mesmo, nós mantemos seu preço baixo, com uma alta qualidade. E então, realizarmos mais um sonho através de nossos trabalhos.</p>
<h3>A melhor opção para um fondue para aniversário de 15 anos</h3>
<p>Nosso fondue para aniversário de 15 anos, pode estar a um passo de você. Portanto, entre em contato com um de nossos especialistas, para que possamos realizar esse desejo o quanto antes. Nossos meios de contatos estão sempre disponíveis a todo momento para que você entre em contato conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>