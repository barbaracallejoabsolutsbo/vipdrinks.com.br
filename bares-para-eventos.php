<?php
    $title       = "Bares para Eventos";
    $description = "Estamos disponíveis a qualquer momento que desejar, para realizarmos o seu orçamento. Entre em contato conosco, para colocarmos em prática tudo o que você deseja";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Para você que deseja ter os melhores atendimentos em seus eventos, para os seus convidados, os nossos serviços de bares para eventos é a melhor opção. Pois a Vipdrinks é uma empresa qualificada para atender a qualquer tipo de pedido e corresponder a qualquer necessidade, superando sempre suas expectativas com nossos bares para eventos. Nossa empresa como um todo, juntamente de nossos profissionais, evoluiu a cada experiência para que nos tornemos a cada dia referência não só com os nossos bares para eventos, mas com todos os serviços que estão disponibilizados em nosso site. Todos os representantes possuem diversas experiências nesse mercado, para que possamos afirmar que você terá um trabalho entregue pelos mesmos, com extrema qualidade. Ideias para eventos renomados vêm crescendo ao longo dos anos e para que tal feito aconteça, é extremamente necessário recorrer a bares para eventos de confiança, para que os seus pedidos sejam prioridades. E os profissionais da Vipdrinks são instruídos para que isso aconteça. Garantimos que em cada fase de nosso atendimento, até mesmo após a prestação de nossos serviços, você saberá que há qualquer momento em que for realizar um evento, a Vipdrinks é o lugar ideal, pois o que você idealiza, nós realizamos. Temos uma grande expansão com os nossos serviços, para que estejamos sempre aptos a qualquer pedido em relação a eventos, que você consultar a nós. Estamos sempre estudando novas técnicas nos métodos utilizados na execução de nossos drinks, para que sejamos cada vez mais únicos, mantendo nosso destaque.</p>

<h2>Mais informações sobre nossos bares para eventos</h2>
<p><br />Ao consultar nossos bares para eventos, você não terá preocupações, pois nós temos todos os recursos para nos adaptarmos a qualquer evento. Desde seu primeiro contato conosco, você terá toda nossa atenção voltada a você, pois queremos ter a sua confiança, para que a qualquer momento que precisar de nossos serviços, você recorra a nós. Desejamos que mais pessoas possam ter acesso ao nosso excelente serviço e para isso, fazemos questão de mantermos nossos valores acessíveis, para que possamos ser a primeira opção nesse ramo.</p>
<h2><br />O melhor lugar para obter bares para eventos</h2>
<p><br />Nos doamos 100% em cada fase de nosso atendimento, para que todos os que de alguma forma, irão usufruir de nossos bares para eventos, possam ter a melhor experiência possível. Entre em contato conosco, para colocarmos em prática tudo o que você deseja. Será um prazer ter você como cliente!</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>