<?php
    $title       = "Bartender para Formatura";
    $description = "Bartender para formatura você encontra na VIP Drinks. Oferecemos contratação de profissionais experientes na preparação de drinks e coquetéis.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Tradicionalmente o serviço de bartender para formatura é um dos maiores sucessos entre os convidados. Sabe porquê? Os bares promovem movimento e diversão a sua volta e para quem o frequenta. Os convidados se sentem a vontade para beberem o que desejar e o serviço de buffet e garçom fica menos sobrecarregado.</p>
<h2>Conheça o serviço de bartender para formatura</h2>
<p>A VIP Drinks oferece a contratação de profissionais experientes na preparação de drinks e atendimento ao público. O bartender para formatura requer habilidade e carisma, além de disposição durante cada preparação. </p>
<p>Para garantir o sucesso do seu evento, contrate um bartender para formatura e nosso serviço de open bar. Os bares recebem muitos convidados sendo locais de referência e interação durante a festa. O local onde ele será colocado também é importante na organização, tornando a formatura mais dinâmica e divertida. </p>
<p>Por isso, o local de open bar inova o ambiente e proporciona entretenimento. Nossa equipe de bartender para formatura é totalmente treinada e profissional para preparar seu drink de forma habilidosa e divertida. Assim, até no momento de aguardo, oferecemos alegria e interação entre convidados e profissionais.</p>
<h3>Vantagens de um bartender para formatura </h3>
<p>Além do entretenimento, o bartender para formatura conhece toda a diversidade e sabores dos drinks preparados. Quaisquer dúvidas sobre a composição das bebidas, ingredientes selecionados, variedades dos produtos, nossos profissionais estarão prontos a responder. </p>
<p>Outro ponto importante para a VIP Drinks é o visual das bebidas. A apresentação é fundamental para chamar atenção dos convidados por isso, oferecemos drinks memorável que darão água na boca. Ainda, disponibilizamos um cardápio sem álcool, contemplando todos os gostos e faixas etárias. </p>
<p>O serviço de bartender para formatura é ideal para quem busca praticidade, conforto e bebidas de qualidade durante todo o evento. Sendo grande atrativo durante toda a festa de formatura, entregando bebidas saborosas e sofisticadas. Oferecemos variedade e decoração apropriada, com temática específica para que desde o visual até os produtos sejam atrativos para os convidados.</p>
<p>Nossos serviços incluem também aniversários, confraternização de empresas, workshops, entre outras comemorações. Estamos cientes da importância que seu evento carrega e da responsabilidade em servir bem a todos que chegam até nós. Preparamos tudo com muito carinho para garantir o sucesso da sua festa.</p>
<p>Conheça mais sobre nossos serviços e entre em contato conosco para solicitar um orçamento. Entregamos qualidade e diversão para que sua formatura seja um momento de diversão e comemoração. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>