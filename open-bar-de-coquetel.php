<?php
    $title       = "Open Bar de Coquetel";
    $description = "Em nosso site está disponibilizado todos os meios de contatos que possuímos para que você entre em contato conosco e então colocarmos em prática, tudo o que você tanto almeja.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Ter um open bar de coquetel em seu evento levará mais sofisticação e renome aos seus convidados, além de tornar tal celebração mais especial para você. Consulte um de nossos profissionais para garantir o nosso serviço de open bar de coquetel, pois sabemos da importância que há em todas as partes de um evento, seja qual for. Nosso open bar de coquetel está disponível no nosso serviço para aniversário, casamento, confraternização de empresas e em demais outros que você poderá consultar em nosso site. Nossos profissionais depositam todos os seus conhecimentos adquiridos atuando em nosso open bar de coquetel, para que entreguemos um serviço de qualidade, cada vez mais. O que faz com que também possamos atender todas as suas necessidades prestadas; não só as suas, mas a de todos os convidados presentes no evento. Seja para fazer um orçamento sem compromisso, ou até mesmo para obter nossos serviços, nosso atendimento a você é o mesmo. Pois queremos fazer com que você tenha a melhor experiência conosco, para que a qualquer momento que precisar de um dos serviços que temos disponíveis, você nos consulte. É um imenso prazer para nós, fazermos parte de momentos únicos na vida de nossos clientes. Até mesmo antes de tal evento, nós reanalisamos cada ítem que são utilizados em nossos drinks e cada processo, para que possamos tornar real, tudo aquilo que é idealizado em sua mente.</p>

<h2>Mais informações sobre o nosso open bar de coquetel</h2>
<p>O nosso serviço de open bar de coquetel é disponibilizado para todos os tipos de eventos, portanto independente da proporção do seu, você poderá ter acesso ao mesmo. Garantimos que ao nos consultar, você não terá nenhum tipo de dor de cabeça, pois temos o comprometimento e o profissionalismo de fazermos tudo o que é necessário para que ocorra tudo de forma correta com os nossos serviços, em seu evento.</p>

<h2>O melhor lugar para obter um open bar de coquetel</h2>
<p>Desde nosso mínimo contato, você se torna nossa maior prioridade. Contudo, buscamos atender a todos os seus pedidos para que você possa ter a melhor experiência com nosso open bar de coquetel. Em nosso site está disponibilizado todos os meios de contatos que possuímos para que você entre em contato conosco e então colocarmos em prática, tudo o que você tanto almeja. Conte sempre com os nossos serviços para que juntos, tornemos esse momento mais especial. Estamos aguardando ansiosamente.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>