<?php

$h1      	 = "Home";
$title    	 = "Vip Drinks";
    $description = "DESCRIÇÃO DO CLIENTE"; // Manter entre 130 a 160 caracteres
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php"; 
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";
    
    $padrao->compressCSS(array(
        "home",
        "tools/nivo-slider",
        "tools/fancybox"
    ));
    
    ?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    
    <main class="main-content">

        <div class="banner-home">
            <div class="theme-default-nivo-slider">
                <div id="slider" class="nivoSlider">
                    <img src="imagens/banner/img-1.jpg" alt="banner" title="Vip Drinks">
                    <img src="imagens/banner/img-2.jpg" alt="banner" title="Vip Drinks">
                    <img src="imagens/banner/img-3.jpg" alt="banner" title="Vip Drinks">
                    <img src="imagens/banner/img-4.jpg" alt="banner" title="Vip Drinks">
                    <img src="imagens/banner/img-5.jpg" alt="banner" title="Vip Drinks">
                </div>
            </div>
        </div> 

        <div class="sobre-nos" id="empresa">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <img class="img-quem-somos" src="imagens/quem-somos.jpg" title="sobre a vip" alt="quem-somos">
                    </div>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                        <img src="imagens/sobre-a-vip.png" title="Sobre a Vip" alt="sobre-a-vip">
                        <p class="text-center">Especializada em eventos sociais, como: casamentos, debutantes e corporativos, a Vip Drinks vem se destacando no mercado com drinks de sabores marcantes e excelência na mão de obra, além de estruturas inovadoras e tecnológicas, com expertise para eventos de intimistas e grande porte.</p>
                        <p class="text-center">Fundada em 2017 pelo bartender e mixologista Gabriel Ornelas, que conta com mais de 10 anos de experiência, atuando desde 2010 no segmento de bares (fixos e eventos) a criação da Vip Drinks bem para melhorar o mercado, trazendo maior qualidade, benefício e diferenciais. Em 2020, com a sociedade com Gabriel Martins, também bartender com longa experiência em eventos, chega ainda mais excelência, qualidade e inovações aos serviços da Vip Drinks.</p>
                        <p class="text-center">Em 2020, inovando mais uma vez, nasce o curso de formação profissional para bartender, para curiosos e pessoas com vontade de ingressar na área.</p>
                        
                    </div>
                </div>
            </div>
        </div>

        <div class="servicos" id="nossos-servicos">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                        <img class="servicos-qualidade" src="imagens/servicos-qualidade.png" title="Serviços e Qualidade" alt="servicos-qualidade">
                        <br>
                        <p class="text-center">Para maior comodidade e um toque que transformam qualquer evento em um momento único, a VipDrinks oferece uma carta de drinks alcoólicos e não alcoólicos, servindo a todos os convidados com eficiência, agilidade, atenção e qualidade.</p>
                        <p class="text-center">Elaboramos opções que destacam-se e se encaixam a todos os gostos e preferências.</p>
                        <br>
                        <img class="title-diferenciais" src="imagens/diferenciais.png" title="Diferenciais" alt="diferenciais">
                        <div class="col-md-6">
                            <ul>
                                <li>Mais de 10 anos de experiência;</li>
                                <li>Profissionais treinados em constante atualização;</li>
                                <li>Estruturas decoradas e com tecnologia;</li>
                                <li>Transporte e mão de obra próprios;</li>
                                <li>Profissionais trajados de acordo ao evento;</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>Profissionalismo e Pontualidade;</li>
                                <li>Menu personalizado com foto;</li>
                                <li>Personalização de pacotes;</li>
                                <li>Degustação individual;</li>
                                <li>Drinks com padrão de qualidade;</li>
                                <li>Frutas frescas.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                        <img class="img-servicos" src="imagens/img-servicos.png" title="Transforme seu evento em um evento unico" alt="img-servicos">
                    </div>
                </div> 
            </div>
        </div>
        <div class="servicos-vip">
            <div class="container">
                <div class="title-servicos-vip">
                    <img class="title-img-vip" src="imagens/servicos-vip.png" title="Serviços Vip" alt="servicos-vip">
                </div>
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-4 col-lg-4">
                        <p class="text-center">Conte com a VipDrinks em seu evento, de pequeno a grande porte estamos prontos para atender!</p>
                        <p class="text-center">Festas em casa, Coquetéis em loja, Casamento, Aniversários ou Eventos Corporativos, a VipDrinks está pronta para te atender!</p>
                        <p class="text-center">Quer uma entrada especial em seu casamento ou evento? Conte com o nosso serviço de Carro de Noiva!</p>
                        <p class="text-center">Veja mais nas nossas redes sociais ou entra em contato para um orçamento detalhado!</p><br>
                        <div class="vip-redes">
                            <br>
                            <a href="https://www.instagram.com/vipdrinks_eventos/"><i class="fab fa-instagram"></i></a><a href="https://www.facebook.com/vipdrinkseventos/"><i class="fab fa-facebook"></i></a>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <a href="https://www.instagram.com/p/CXKMPkkuouJ/"><img src="imagens/servicos/aniversarios.png" title="Aniversarios" alt="aniversarios">
                                    <p class="text-center">Aniversarios</p>
                                </a>
                                <!-- <a href="https://www.instagram.com/p/CDjSkY5hAJb/"><img src="imagens/servicos/masterclass.png" title="Masterclass" alt="masterclass">
                                    <p class="text-center">Masterclass</p>
                                </a> -->
                            </div>
                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <!-- <a href="https://www.instagram.com/p/CXKMPkkuouJ/"><img src="imagens/servicos/aniversarios.png" title="Aniversarios" alt="aniversarios">
                                    <p class="text-center">Aniversarios</p>
                                </a> -->
                                <a href="https://www.instagram.com/p/CDjSkY5hAJb/"><img src="imagens/servicos/masterclass.png" title="Masterclass" alt="masterclass">
                                    <p class="text-center">Masterclass</p>
                                </a>
                            </div>

                            

                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <a href="https://www.instagram.com/p/CTx5vIXrkmZ/"><img src="imagens/servicos/casamento.png" title="Casamentos" alt="casamentos">
                                    <p class="text-center">Casamentos</p>
                                </a>
                                <!-- <a href="https://www.instagram.com/p/B5StQlPhQ4n/"><img src="imagens/servicos/coquetel-de-lojas.png" title="Coquetel de Lojas" alt="coquetel-de-lojas">
                                    <p class="text-center">Coquetel de Lojas</p>
                                </a> -->
                            </div>
                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <!-- <a href="https://www.instagram.com/p/CTx5vIXrkmZ/"><img src="imagens/servicos/casamento.png" title="Casamentos" alt="casamentos">
                                    <p class="text-center">Casamentos</p>
                                </a> -->
                                <a href="https://www.instagram.com/p/B5StQlPhQ4n/"><img src="imagens/servicos/coquetel-de-lojas.png" title="Coquetel de Lojas" alt="coquetel-de-lojas">
                                    <p class="text-center">Coquetel de Lojas</p>
                                </a>
                            </div>

                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <a href="https://www.instagram.com/p/CLcq6bPhUFO/"><img src="imagens/servicos/festa-em-casa.png" title="Festa em Casa" alt="festa-em-casa">
                                    <p class="text-center">Festa em Casa</p>
                                </a>
                                <!-- <a href="https://www.instagram.com/vipdrinks_eventos/"><img src="imagens/servicos/vip-carros-de-noivas.png" title="Vip Carros de Noivas" alt="vip-carros-de-noivas">
                                    <p class="text-center">Vip Carro de Noivas</p>
                                </a> -->
                            </div>
                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <!-- <a href="https://www.instagram.com/p/CLcq6bPhUFO/"><img src="imagens/servicos/festa-em-casa.png" title="Festa em Casa" alt="festa-em-casa">
                                    <p class="text-center">Festa em Casa</p>
                                </a> -->
                                <a href="https://www.instagram.com/vipdrinks_eventos/"><img src="imagens/servicos/vip-carros-de-noivas.png" title="Vip Carros de Noivas" alt="vip-carros-de-noivas">
                                    <p class="text-center">Vip Carro de Noivas</p>
                                </a>
                            </div>

                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <a href="https://www.instagram.com/p/B6V5gC7h3mu/"><img src="imagens/servicos/eventos-corporativos.png" title="Eventos Corporativos" alt="eventos-corporativos">
                                    <p class="eventos text-center">Eventos Corporativos</p>
                                </a>
                                <!-- <a href="https://www.instagram.com/p/B3XiZLUBf-m/"><img src="imagens/servicos/eventos-litoral.png" title="Eventos Litoral" alt="eventos-litoral">
                                    <p class="text-center">Eventos no Litoral ou Forda de SP</p>
                                </a> -->
                            </div>
                            <div class="servicos-prestados col-xs-12 col-sm-3 col-md-3 col-lg-3">
                                <!-- <a href="https://www.instagram.com/p/B6V5gC7h3mu/"><img src="imagens/servicos/eventos-corporativos.png" title="Eventos Corporativos" alt="eventos-corporativos">
                                    <p class="eventos text-center">Eventos Corporativos</p>
                                </a> -->
                                <a href="https://www.instagram.com/p/B3XiZLUBf-m/"><img src="imagens/servicos/eventos-litoral.png" title="Eventos Litoral" alt="eventos-litoral">
                                    <p class="text-center">Eventos no Litoral ou Forda de SP</p>
                                </a>
                            </div>






                        </div>
                    </div>



                    
                            
                                    
                                            
                                                </div>
                                            </div>
                                        </div>
                                        <div class="carro-noiva">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-5 col-md-5 col-lg-5">
                                                        <a href="https://www.instagram.com/p/CYZ9z4ZIwX4/"><img src="imagens/casamento-1.png" title="Vip carros Noivas" alt="casamento-1"></a>
                                                        <a href="https://www.instagram.com/vipdrinks_eventos/"><img src="imagens/casamento-2.png" title="Vip Carros Noivas" alt="casamento-2"></a>
                                                        <a href="https://www.instagram.com/p/CUJDogkrF3R/"><img src="imagens/casamento-3.png" title="Vip Carros Noivas" alt="casamento-3"></a>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-7 col-md-7 col-lg-7">
                                                        <img class="carro-noiva-title" src="imagens/vip-carro-noiva.png" title="Vip Carros Noiva" alt="vip-carro-vip">
                                                        <h3 class="text-center">Quer fazer da entrada em seu evento um momento único?!
                                                            <br>Então conte com a Vip Carro Noivas para isso!</h3>
                                                            <p class="text-center">A entrada do seu evento pode ser ainda mais especial, com um maravilhoso carro de luxo, equipamento com ar-condicionado, bancos de couro e teto solar. Com motorista de traje executivo fino completo e decoração especial para casamentos, você tem uma entrada única, glamurosa e que chama a atenção de todos durante o trajeto!</p>
                                                            <p class="text-center">Atendemos Casamentos, Debutantes, Eventos e Executivos, sempre com muito requinte, segurança, comodidade e elegância, e o máximo de pontualidade!</p>
                                                            <br>
                                                            <h2 class="text-center">SOBRE O SERVIÇO:</h2>
                                                            <p class="text-center">Ao contratar o serviço, você conta com:</p>
                                                            <ul>
                                                                <li>Motorista bilíngue com traje executivo fino;</li>
                                                                <li>Translados de acordo com o mapa do evento*;
                                                                    <p class="comentario">*Pré-definidos no momento da contratação</p>
                                                                </li>
                                                                <li>Carro disponível para sessão de fotos;</li>
                                                                <li>Placa personalizada de cortesia*
                                                                    <p class="comentario">*Para contratos com 20 dias de antecedência</p>
                                                                </li>
                                                            </ul>
                                                            <br>
                                                            <h2 class="text-center">SOBRE O CARRO:</h2>
                                                            <p class="text-center">Opirus Preto, Carro de alto padrão, do mesmo nível da linha Jaguar S-Type. Com teto solar, bancos de couro e ar-condicionado.</p>
                                                            <p class="text-center">Modelo Importado, vieram apenas 27 exemplares ao todo no Brasil. Um carro elegante e rato.</p>
                                                            <br>
                                                            <br>
                                                            <a class="insta-carro-noiva text-center" href="https://www.instagram.com/vipdrinks_eventos/" title="Instagram"><i class="fab fa-instagram"></i><br>Veja Mais Em Nosso Instagram</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="contato" id="contato">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <img class="contato-title" src="imagens/title-contato.png" alt="title-contato" title="Contato">
                                                            <br>
                                                            <p class="text-center">Quer contratar nossa equipe, tirar dúvidas ou fazer um orçamento?</p>
                                                            <br>
                                                            <p class="text-center">Preencha o formulário ao lado e nossa equipe entrará em contato o mais breve possível!</p>
                                                            <p class="text-center">Se você é Bartender e quer fazer parte da nossa equipe, entre em contato através do nosso e-mail contato@vipdrinks.com.br.</p>
                                                            <p class="text-center">Nos diga também nas redes sociais e veja mais sobre nossos eventos, drinks e equipe!</p>
                                                            <div class="col-md-6">
                                                                <img class="logo-contato" src="imagens/logo-contato.png" title="Vip Drinks" alt="logo-footer">
                                                            </div>
                                                            <div class="redes-contato col-md-6">
                                                                <a class="contato-redes" href="https://www.facebook.com/vipdrinkseventos/" title="Facebook"><i class="fab fa-facebook"></i> Vip Drinks</a><br>
                                                                <br>
                                                                <a class="contato-redes" href="https://www.instagram.com/vipdrinks_eventos/" title="Instagram"><i class="fab fa-instagram"></i> @vipdrinks_eventos</a><br>
                                                                <br>
                                                                <a class="contato-redes" href="<?php echo $unidades[1]["whatsapp-link"]; ?>" title=""><i class="fab fa-whatsapp"></i> (11) 95388-4482</a>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                            <form class="form-contato">
                                                                <div class="form-group">
                                                                    <label>Nome: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][nome]" type="text" class="form-control"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>E-mail: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][email]" type="email" class="form-control"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Telefone: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][telefone]" type="text" class="form-control mask-phone" placeholder="(__)____-____"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Assunto: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][assunto]" type="text" class="form-control"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Local da Festa: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][local_festa]" type="text" class="form-control"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Quantidade de Convidados: <span class="red-color">*</span></label>
                                                                    <b><input name="data[Contato][qtd_convidados]" type="text" class="form-control"></b>
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Mensagem: <span class="red-color">*</span></label>
                                                                    <b><textarea name="data[Contato][mensagem]" class="form-control" rows="6"></textarea></b>
                                                                </div>
                                                                <?php if($captcha){ ?>
                                                                    <div class="g-recaptcha" data-sitekey="<?php echo $captcha_key_client_side; ?>"></div>
                                                                <?php } ?>
                                                                <div class="text-right">
                                                                    <button type="submit" class="btn btn-default btn-block"><b>Enviar</b></button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> 
                                            <div class="tranforme-seu-evento">
                                                <div class="em-um-momento-unico">
                                                    <div class="container">
                                                        <img src="imagens/transforme-seu-evento.png" title="Transforme seu Evento em um Momento Único" alt="transforem-seu-evento">
                                                    </div>
                                                </div>
                                            </div>

                                            
                                        </main>
                                        <?php include "includes/btn-fixos.php"; ?>

                                        <?php include "includes/_footer.php"; ?>

                                        <?php $padrao->compressJS(array(
                                            "tools/bootstrap.min",
                                            "tools/jquery.validate.min",
                                            "tools/jquery.mask.min",
                                            "tools/jquery.fancybox",
                                            "jquery.padrao.contact",
                                            "tools/jquery.nivo"
                                        )); ?>

                                        <script>
                                            $(function(){
                                                $("#slider").nivoSlider();
            //effect: "random",
            //slices: 15,
            //boxCols: 8,
            //boxRows: 4,
            //animSpeed: 500,
            //pauseTime: 3000,
            //startSlide: 0,
            //directionNav: true,
            //controlNav: true,
            //controlNavThumbs: false,
            //pauseOnHover: true,
            //manualAdvance: false,
            //prevText: 'Prev',
            //nextText: 'Next',
            //randomStart: false,
            //beforeChange: function(){},
            //afterChange: function(){},
            //slideshowEnd: function(){},
            //lastSlide: function(){},
            //afterLoad: function(){}
        });
    </script>
    <a id="trigger-mensagem-fancybox" href="#modal-mensagem-fancybox" style="display:none"></a>
    <div id="modal-mensagem-fancybox" class="text-center"></div>
    <?php if($captcha){ ?>
        <script src="https://www.google.com/recaptcha/api.js"></script>
    <?php } ?>
    <script>
        $(function(){
            var form_identification = ".form-contato";
            $(form_identification).validate({
                errorClass: "control-label",
                validClass: "control-label",
                rules: {
                    "data[Contato][nome]" : {
                        required: true
                    },
                    "data[Contato][email]" : {
                        required: true,
                        email: true
                    },
                    "data[Contato][telefone]" : {
                        required: true
                    },
                    "data[Contato][assunto]" : {
                        required: true
                    },
                    "data[Contato][local_festa]" : {
                        required: true
                    },
                    "data[Contato][qtd_convidados]" : {
                        required: true
                    },
                    "data[Contato][mensagem]" : {
                        required: true
                    }
                },
                highlight: function (element){
                    $(element).parents("div.form-group").addClass("has-error").removeClass("has-success");
                }, 
                unhighlight: function (element){ 
                    $(element).parents("div.form-group").removeClass("has-error").addClass("has-success"); 
                },
                submitHandler: function(form){
                    var dados = $(form_identification).serialize();
                    $.ajax({
                        type: "POST",
                        url: "includes/dispara-email.php",
                        data: dados,
                        dataType: "json",
                        beforeSend: function(){
                            $(".btn-send").html("Aguarde...").attr("disabled", "disabled");
                            $(".form-control").prop("disabled", true);
                        },
                        success: function(data){
                            if(data.status){
                                window.location = "envia-contato";
                            }else{
                                $("#modal-mensagem-fancybox").html(data.message);
                                $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                            }
                        },
                        error: function(data){
                            $("#modal-mensagem-fancybox");
                            $("#trigger-mensagem-fancybox");
                            if(data.status){
                                window.location = "envia-contato";
                            }else{
                                $("#modal-mensagem-fancybox").html(data.message);
                                $("#trigger-mensagem-fancybox").fancybox().trigger("click");
                            }
                        },
                        complete: function(){
                            $(".btn-send").html("Enviar").removeAttr("disabled");
                            $(".form-control").prop("disabled", false);
                        }
                    });
                }
            });
        });
    </script>
    
</body>
</html>