<?php
    $title       = "Serviço de Bar em São Paulo";
    $description = "Será um prazer podermos levar nossos incríveis trabalhos ao seu evento, tornando-o inesquecível em mais um aspecto. Não perca essa incrível oportunidade.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Você pode consultar o nosso serviço de bar em São Paulo a qualquer momento, pois estamos sempre disponíveis para te atender. Seja para qual for o evento, sabemos da importância que é obter um serviço de bar em São Paulo, com drinks renomados e sofisticados. Temos o objetivo de nos tornarmos a cada dia, cada vez mais referência com nosso serviço de bar em São Paulo, com todo o conhecimento absorvido pelos nossos profissionais com suas experiências, aprimorando também nossa qualidade. Desde seu primeiro contato conosco, nossos profissionais mantêm toda a atenção necessária a você, pois sabemos a importância que é sair como tudo foi planejado, em seu evento. Portanto, nós correspondemos todas as suas necessidades e pedidos, nos adaptando e moldando sempre, ao que nos é solicitado. Ao navegar em nosso site, você poderá ver algumas imagens dos nossos serviço de bar em São Paulo e também, fazer o seu orçamento de forma online e rápida perante ao mesmo. Nós possuímos esse e demais outros serviços que também são disponíveis a você, a qualquer momento. Pois nós atendemos a qualquer tipo de evento, como confraternização de empresas, aniversários, casamentos e demais outros. Portanto, nossos serviços são disponíveis a qualquer um dos que nos procuram.</p>

<h2>Mais informações sobre o nosso serviço de bar em São Paulo</h2>
<p>A cada ano de nossa atuação nesse ramo, nós adquirimos mais conhecimento e experiência, fazendo com que a qualidade de nosso serviço de bar em São Paulo seja cada vez mais alta. E por esse e demais motivos que temos a total convicção em argumentamos isso a você. Para que em seu evento, você e os convidados tenham incríveis experiências, consulte a Vipdrinks, para que os nossos drinks façam tal momento, mais especial e único. Nós mantemos a qualidade de nossos produtos, porém com um preço baixo, para que nossos clientes não tenham nenhum prejuízo financeiro ao nos consultar. Pois também, queremos fazer com que todos tenham acesso aos drinks de mais alta qualidade da região.</p>

<h2>O melhor lugar para obter o nosso serviço de bar em São Paulo</h2>
<p>Nossos meios de contatos estão disponíveis a qualquer momento para que você entre em contato com nossos especialistas e tire qualquer dúvida que possuir sobre o nosso serviço de bar em São Paulo. Será um prazer podermos levar nossos incríveis trabalhos ao seu evento, tornando-o inesquecível em mais um aspecto. Não perca essa incrível oportunidade.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>