<?php
    $title       = "Bartender para Festas";
    $description = "Bartender para festas é com a VIP Drinks! Conheça nossos segmentos que incluem também aniversários, confraternização de empresas, casamentos, e muito mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Garanta o sucesso do seu evento com uma equipe de bartender para festas. A VIP Drinks trabalha com os melhores profissionais: experientes, simpáticos e atenciosos. Além de preparar ótimos drinks, proporcionam diversão e carisma durante a sua festa.</p>
<h2>Importância de um bartender para festas</h2>
<p>Uma das maiores preocupações em eventos festivos são as comidas e bebidas servidas para os convidados. Para que esse momento seja também de alegria conheça nosso serviço de bartender para festas. Preparados para tirar todas as suas dúvidas quanto à composição de bebidas, escolha dos ingredientes, preferencia de sabores, nosso bartender para festas possuem experiência com atendimento ao cliente.</p>
<p>Os bares são atrações importantes, os quais recebem os convidados durante toda comemoração. Nosso bartender para festas está preparado para servir os melhores drinks com muito profissionalismo e maestria. Esse serviço exige disposição e carisma, colocando-os em movimento durante um longo período. Além disso, os convidados se sentem mais a vontade para pedir bebidas com o bartender para festas, pois eles entendem do assunto e conhecem o cardápio. </p>
<p>Por isso, se você procura por um serviço especializado em open bar com profissionais qualificados, encontrou o lugar certo.</p>
<h3>Conheça a VIP Drinks e nossos serviços</h3>
<p>Somos uma empresa especializada em serviços de open bar e preparação de bebidas. Tornamos nosso bar sofisticado e personalizado para cada evento, seja ele mais simples ou elegante. Por isso, nosso bartender para festas está sempre preparado para qualquer segmento comemorativo, entre eles, festas de aniversário, baile de debutante, casamentos, confraternizações, workshops e assim por diante.</p>
<p>Além da diversidade e sabores dos drinks, outro ponto importante para a VIP Drinks é a apresentação das bebidas. O visual é o primeiro passo para chamar a atenção, por isso, além do sabor, oferecemos uma apresentação memorável que dará ainda mais água na boca. </p>
<p>Assim, o serviço de bartender para festas conta com três principais fatores: bebidas saborosas, drinks que chama a atenção antes mesmo de serem provados, e diversidade nas escolhas de ingredientes e especiarias.</p>
<p>Para conquistar a todos, nosso cardápio se estende com bebidas de alta qualidade, marcas confiáveis e variações na escolha de ingredientes. Serão conquistados desde os paladares mais cítricos até os mais doces. Ainda, disponibilizamos um cardápio sem álcool, contemplando todos os gostos e faixas etárias.</p>
<p>Conheça mais sobre nossos serviços e entre em contato conosco para solicitar um orçamento. Estamos aqui para fazer parte da comemoração dos seus sonhos.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>