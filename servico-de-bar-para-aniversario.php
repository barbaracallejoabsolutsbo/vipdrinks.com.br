<?php
    $title       = "Serviço de Bar para Aniversário";
    $description = "Todos os nossos meios de contatos estão sempre disponíveis para que você possa falar com qualquer um de nossos profissionais. Entre em contato conosco o quanto antes, para podermos realizar o seu sonho de ter um dia incrível e memorável.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Não só o nosso serviço de bar para aniversário, mas todos os nossos serviços prestados para diversos eventos, são executados com extrema qualidade e profissionalismo para que possamos realizar tudo o que é desejado por você. Todas as nossas experiências, nos adaptando a cada cliente, fizeram com que hoje, tenhamos grande conhecimento e habilidades nesse ramo, para que possamos nos tornar cada vez mais referência com nosso serviço de bar para aniversário. Tanto pela qualidade que há em nossos drinks, quanto pelo nosso incrível atendimento. Nós queremos fazer com que mais e mais pessoas tenham acesso ao nosso serviço de bar para aniversário e para isso, nós nos adaptamos a qualquer ideia que chegar até nós, para que possamos realizar mais sonhos. Portanto, nos traga a sua ideia, para que analisemos com cautela e então, realizarmos nosso atendimento personalizado a você. Não perca a oportunidade de receber o melhor serviço de bar para aniversário, pois sabemos o quão único esse momento é. E com um bom drink, ele se torna ainda mais inesquecível. Lembrando que você pode nos consultar a qualquer momento para diferentes tipos de trabalhos, então mesmo após o seu aniversário, você terá outras oportunidades de usufruir de nossos serviços, pois nós atuamos em eventos como: confraternização de empresas, casamento, festa de debutante e demais outros. E afirmamos que após a sua primeira experiência conosco, você desejará mais, mais vezes.</p>
<h2>Mais detalhes sobre nosso serviço de bar para aniversário</h2>
<p>O valor do nosso serviço de bar para aniversário, é de acordo com a qualidade do mesmo, porém isso não significa que não são acessíveis. Faça o seu orçamento persnalizado em nosso site, ou entre em contato com um de nossos especialistas para fazer o mesmo e também tirar possíveis dúvidas que possuir. Será um prazer realizarmos o seu sonho de ter um dia incrível.</p>
<h2>A melhor opção para um serviço de bar para aniversário</h2>
<p>Temos o comprometimento em entregarmos nossa mais alta qualidade no nosso serviço de bar para aniversário, para que quando você necessitar desse ou demais outros serviços que nós disponibilizamos, a Vipdrinks seja sua primeira e única opção. Temos o prazer em levarmos nossos trabalhos para os mais variados eventos. Todos os nossos meios de contatos estão sempre disponíveis para que você possa falar com qualquer um de nossos profissionais. E temos também nossas redes sociais, onde você pode acompanhar os nossos trabalhos de perto. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>