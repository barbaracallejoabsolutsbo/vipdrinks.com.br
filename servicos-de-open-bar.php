<?php
    $title       = "Serviços de Open Bar";
    $description = "Oferecemos serviços de open bar para sua festa. A VIP Drink conta com profissionais experientes na preparação de bebidas exclusivas aos seus convidados. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Os serviços de Open Bar são cada vez mais solicitados para fornecer uma atração diferenciada em eventos. Sendo uma ótima opção para a sua festa, leva dinamismo e diversão para encantar os convidados.</p>
<p>Mais que um serviço de preparação de coquetéis, o open bar é um atrativo durante toda a comemoração e proporciona um cardápio ideal com drinks de sua preferência.</p>
<h2>Conheça algumas vantagens dos serviços de Open Bar</h2>
<p>Uma das maiores preocupações em festas e eventos são as bebidas. Os serviços de Open Bar permitem uma variedade de drinks para agradar a todos os paladares. Os menus podem ser personalizados conforme a preferência do cliente que o contrata, adicionando assim bebidas não alcoólicas, por exemplo. Esse recurso torna os serviços de Open Bar inclusivos e diferenciados dos tradicionais buffets.</p>
<p>Outra diferença é a dinamicidade do evento. Com um bar montado conforme a temática do local, os convidados se dirigem até os bartenders. Esse movimento propõe maior agitação a comemoração e permite uma liberdade maior de escolha das pessoas, assim elas se sentem mais confortáveis para beber o que desejarem.</p>
<p>Os serviços de Open Bar também levam diversão para sua festa. Os bartenders, mais que profissionais especializados na preparação de bebidas, podem ter experiência em malabarismos e atrações divertidas durante o serviço. Assim, os convidados são entretidos enquanto esperam suas bebidas. E mais, o ambiente do bar se torna um local de interação e encontro durante as festas.</p>
<h3>O que esperar dos nossos serviços de Open Bar</h3>
<p>Estamos preparados para atender eventos de diferentes proporções. Desde formaturas, casamentos, festas de aniversário, até confraternizações, entre outros. Por isso, se você procura por serviços de Open Bar que levarão qualidade e diversão, a VIP Drinks oferece isso a você!</p>
<p>Nossa equipe é experiente e treinada para atender cada convidado com atenção e animação. Os barmen precisam estar familiarizados com o ritmo do trabalho que exige carisma e energia. Além do atendimento, colocamos a organização do espaço e apresentação das bebidas como pontos fundamentais de nosso trabalho. </p>
<p>Os serviços de Open Bar da VIP Drink contam com profissionais que atuam com vasto conhecimento em preparação de bebidas e atendimento em eventos. Para saber mais sobre os nossos serviços, tirar dúvidas ou solicitar um orçamento, entre em contato conosco e conheça nossa equipe. </p>
<p>Estamos dispostos a fazer parte da comemoração dos seus sonhos levando o melhor atendimento a você e a todos seus convidados.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>