<?php
    $title       = "Fondue Para Festa em São Paulo";
    $description = "Não perca a oportunidade de obter o fondue para festa em São Paulo da Vipdrinks. Será um prazer para nós, fazermos partes de um momento único e especial.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>A Vipdrinks possui os melhores recursos para então oferecer o melhor fondue para festa em São Paulo. E por esse e outros motivos, nós somos a sua melhor opção para garantir o mesmo. A Vipdrinks possui longos anos de experiência em fondue para festa em São Paulo e demais serviços para eventos, onde nos especializamos para então entregarmos um serviço cada vez mais qualificado. Atribuímos diversos conhecimentos para que possamos atender a diversos tipos de eventos. Portanto independente da proporção e qual evento você deseja colocar nosso fondue para festa em São Paulo, o mesmo é totalmente apto a você. Eventos como: aniversários, casamentos, eventos corporativos, confraternização de empresas e demais outros que você poderá conhecer mais detalhadamente em nosso site. O nosso fondue para festa em São Paulo, será um diferencial que surpreenderá a você e aos seus convidados. Pois não é somente um fondeu, o nosso fondue para, é apto para qualquer pedido, pois utilizamos dos melhores materiais e dos mais variados alimentos, para que possamos atender todos da melhor forma. E o mesmo fará com que você e seus convidados tenham memórias incríveis de serem recordadas, de seu dia mais que especial. Um dos nossos maiores objetivos é podermos realizar sonhos através de nossos trabalhos. Portanto, não perca a oportunidade de poder usufruir de nossos trabalhos. Navegue em nosso site para que você conheça todos os serviços que temos disponíveis para diferente festas e eventos, podendo adquirir nosso fondue ou demais serviços.</p>

<h2>Conheça mais sobre nosso fondue para festa em São Paulo</h2>
<p>Aprimoramos e melhoramos nossos serviços a cada dia mais, para que nos tornemos cada vez mais referência com o nosso fondue para festa em São Paulo e nossos demais serviços. Nós nos entregamos 100% em cada fase de nosso atendimento, desde seu primeiro contato, para que sempre que necessitar de serviços para eventos e festas, você já saiba que a Vipdrinks é o único lugar onde todos os seus pedidos serão correspondidos. Nos preocupamos não só com que você tenha experiências incríveis com os nossos serviços, mas todos os convidados de sua festa. Portanto, n]ao hesite em nos constatar.</p>

<h3>O melhor lugar para adquirir um fondue para festa em São Paulo</h3>
<p>Entre em nosso site para fazer o seu orçamento perante ao nosso fondue para festa em São Paulo. Todos os nossos meios de contatos estão sempre disponíveis para que você possa falar conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>