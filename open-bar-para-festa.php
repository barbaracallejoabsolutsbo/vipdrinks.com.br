<?php
    $title       = "Open Bar para Festa";
    $description = "Nosso open bar para festa atenderá todas as necessidades que você possuir e as de seus convidados também. Consulte a Vipdrinks o quanto antes ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Ao procurar por um open bar para festa de qualidade, a Vipdrinks é o único lugar que pode fornecer esse serviço com a qualidade que você busca. Nós fornecemos esse e demais serviços para diversos lugares. Portanto, consulte um de nossos profissionais para que possamos fazer um atendimento personalizado a você. Um dos nossos maiores objetivos é fazer com que nosso Open bar para festa esteja presente em seu evento, para que seus convidados tenham incríveis memórias com os mesmos. Em nosso Open bar para festa há drinks exclusivos e de grande requinte que com certeza surpreenderá você e o seus convidados. Vale lembrar que o nosso open bar para festa pode ser posto em eventos como, casamentos aniversários, confraternização de empresas, entre outros. Nós utilizamos sempre dos melhores materiais para que a nossa qualidade esteja explícita para todos aqueles que usufruíram de nossos serviços. Estamos sempre buscando novas técnicas e inovando em nossos open bar para festa, para atendermos a qualquer pedido que chegar até nós. Como parte de nossa ética, nós entregamos sempre nosso melhor atendimento desde seu primeiro contato conosco, para que a qualquer momento que você necessitar de qualquer serviço para evento nós sejamos a sua primeira opção. Não perca essa oportunidade de adquirir nossos trabalhos para que o seu evento seja único para você e para o seus convidados. Garantimos que a sua experiência conosco ficará para sempre em sua memória fazendo com que você queira revivê-la diversas vezes. Fazemos sempre um atendimento personalizado para que todos os seus objetivos e desejos perante aos nossos serviços sejam concretizados da forma como você idealizou. </p>

<p>Mais detalhes sobre o nosso Open bar para festa </p>
<p>Desde o princípio de nossa empresa não só o nosso open bar para festa mas como todos os nossos serviços são extremamente acessíveis ao público, pois sabemos como tal é um diferencial em seu evento e da importância que é obtê-lo. Portanto realize o seu orçamento em nosso site de forma on-line para que você o garanta o quanto antes. Mas caso prefira entrar em contato diretamente com um de nossos profissionais. </p>

<p>A melhor opção de open bar para festa</p>
<p>Em nosso site há diversos meios de contatos para que você possa falar com um de nossos especialistas e tirar qualquer dúvida que possuir perante ao nosso open bar para festa. Conte sempre com os serviços da vipdrinks. Te aguardamos. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>