<?php
    $title       = "Bartender para Drinks sem álcool";
    $description = "A VIP Drinks dispõe de bartender para drinks sem álcool. Conheça nossos segmentos que incluem aniversários, casamentos, workshops, e muito mais.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Uma das grandes preocupações na organização de um evento são as comidas e bebidas servidas. Seja uma festa grande com inúmeros convidados, ou uma festa pequena com número mais restrito, a satisfação dos convidados é que definirá seu sucesso.</p>
<p>Em comemorações envolvendo diversos públicos e faixas etárias — como aniversários e festas de casamento —  é prudente um serviço de bartender para drinks sem álcool. </p>
<h2>Conheça o serviço de bartender para drinks sem álcool</h2>
<p>A VIP Drinks disponibiliza um cardápio apenas com bebidas não alcoólicas. Assim, no serviço de bartender para drinks sem álcool você e seus convidados poderão degustar sem se preocupar com a idade.</p>
<p>Oferecemos uma estrutura completa e personalizada para a sua comemoração. O bartender para drinks sem álcool conta com diversos modelos de bares, diversidades de drinks e temáticas que combinarão com sua festa. Nossos bartenders são treinados para proporcionar os melhores drinks e atrativos durante cada preparação. Mais que sabor, nossas bebidas possuem apresentação. </p>
<p>Assim, o bartender para drinks sem álcool conta com três principais fatores: bebidas saborosas, drinks que chamam a atenção antes mesmo de serem provados, e diversidade nas escolhas de ingredientes e especiarias.</p>
<p>Mais que um serviço, o bartender para drinks sem álcool é um ponto de atração da festa, onde os convidados costumam se reunir para interação e diversão.</p>
<h3>Conheça a VIP Drinks e nossos serviços</h3>
<p>Trabalhamos de maneira comprometida para garantir um serviço de bartender para drinks sem álcool de qualidade. Para isso, utilizamos bebidas de marcas confiáveis, além dos melhores ingredientes, bem selecionados. </p>
<p>Somos uma empresa com serviço especializado em open bar e preparação de drinks. Além do bartender para drinks sem álcool, você pode encontrar bartender para festa de debutante, festa de casamento, bar de caipirinhas, bar de coquetéis, e muito mais! </p>
<p>Nossa equipe é composta por profissionais experientes que conhecem a qualidade de um bom atendimento ao cliente. De forma respeitosa, habilidosa e profissional tornamos seu evento inesquecível!  Tudo para que a satisfação de clientes e convidados seja conquistada.</p>
<p>Nossos serviços incluem também aniversários, confraternização de empresas, workshops, entre outras comemorações. Estamos cientes da importância que seu evento carrega e da responsabilidade em servir bem a todos que chegam até nós. Preparamos tudo com muito carinho para garantir o sucesso da sua festa.</p>
<p>Conheça mais sobre nossos serviços e entre em contato conosco para solicitar um orçamento. Estamos aqui para fazer parte da comemoração dos seus sonhos. </p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>