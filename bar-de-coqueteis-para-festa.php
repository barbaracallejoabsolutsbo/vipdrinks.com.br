<?php
    $title       = "Bar de Coquetéis para Festa";
    $description = "Nossos profissionais são altamente qualificados e especializados em bar de coquetéis para festa. Potanto, garantimos que você não se arrependerá de sermos sua primeira opção. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>O serviço de bar de coquetéis para festa é perfeito para quem busca praticidade, conforto e bebidas de qualidade durante um evento festivo. A VIP Drinks possui toda a estrutura de bar e profissionais especializados para atuarem na preparação de coquetéis e atendimento aos convidados.</p>
<p>Os bares são atrações importantes nas festas, já que promovem movimento e diversão durante todo o evento. Os convidados se sentem a vontade para beberem o que desejar e o serviço de garçom fica menos sobrecarregado.</p>
<h2>Especializados em bar de coquetéis para festa</h2>
<p>Montamos uma estrutura ideal para o bar de coquetéis para festa seja em salão de festas, praia, domicílio, clubes, e assim por diante. Proporcionamos uma atração tanto no visual como na variedade de produtos ofertados. Ao contratar nosso serviço você monta o cardápio de sua preferência, com drinks e bebidas que agradem todos os gostos.</p>
<p>No bar de coquetéis para festa os convidados podem fazer quantos pedidos quiserem. Com maior liberdade de escolha do que consumirem, todos os drinks são feitos com bebidas de qualidade e ingredientes confiáveis. Independente do motivo da festa, contamos com bebidas não alcoólicas, para assim agradar a todos os paladares e faixas etárias.</p>
<h3>Bar de coquetéis para festa garante qualidade e diversão</h3>
<p>O bar também faz parte da festa, por isso disponibilizamos uma linha temática para que a decoração combine com o ambiente da comemoração. Sendo um local de interação e conversas, o espaço onde o bar de coquetéis para festa ficará é importante ponto de referência no ambiente. </p>
<p>Geralmente estar mais perto da pista de dança, atrai mais pessoas e proporciona uma combinação perfeita. Tornando a comemoração mais dinâmica e movimentada, o bar de coquetéis para festa é uma atração fundamental que leva o convidado a se deslocar até o serviço. </p>
<p>No próprio bar de coquetéis para festa ele irá encontrar nossos bartenders prontos para atendimento rápido, divertido e profissional. Mais que o sabor, nossos profissionais se preocupam também com a apresentação de cada bebida. E mais, eles estão prontos para tirar dúvidas de qualquer drink e composição de bebidas que surgirem.</p>
<p>Colocamos o interesse de nossos clientes em primeiro lugar. Sendo assim, nossos bares contam com três principais fatores: o sabor dos drinks, a grande variedade de opções e o visual de cada bebida. Assim você e seus convidados experimentarão uma ótima noite de festa. Entregamos qualidade e diversão para que seu evento seja inesquecível. </p>


                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>