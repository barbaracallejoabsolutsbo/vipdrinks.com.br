<?php
    $title       = "Serviço de Bartender para Festa de 15 Anos";
    $description = "Conheça o serviço de bartender para festa de 15 anos da VIP Drinks. Contamos com equipe totalmente profissional para atender seus convidados.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>O momento de uma festa de 15 anos é de grande planejamento, exigindo atenção e dedicação antes e durante a comemoração. Esse evento ficará marcado na memória da aniversariante, dos pais e convidados.</p>
<p>A contratação de um serviço de bartender para festa de 15 anos tornará o momento ainda mais especial e ajudará com a organização da festa. </p>
<h2>Conheça a VIP Drinks e nosso serviço de bartender para festa de 15 anos </h2>
<p>Trabalhamos com segmento para festas que disponibiliza bar de caipirinhas, bar de coquetéis, serviço de open bar, e muito mais. Contamos com um cardápio totalmente personalizado conforme o desejo de cada cliente para o serviço de bartender para festa de 15 anos.</p>
<p>Salientamos a presença de uma gama de drinks não alcoólicos para contemplar todos os convidados, compondo assim um cardápio heterogêneo e adequado a todas as faixas etárias.</p>
<p>Nosso serviço de bartender para festa de 15 anos conta com profissionais comprometidos e experientes, realizando um atendimento animado durante toda a festa. Mais que um serviço, o bar funciona como atração e estabelece um local de interação entre os convidados. </p>
<p>Dessa forma, nossa equipe esta preparada e ciente de uma excelente qualidade de atendimento ao cliente. Somos responsáveis por tirar dúvidas sobre a composição dos drinks, ingredientes, especiarias e combinações de bebidas.</p>
<h3>O serviço de bartender para festa de 15 anos  deixa tudo mais divertido</h3>
<p>Uma das principais vantagens em contratar o serviço de bartender para festa de 15 anos é a qualidade das bebidas e oportunidade dos convidados em conhecer bebidas diferentes e exclusivas. Esse atrativo torna o evento mais agradável e divertido, satisfazendo a todos que apreciam uma boa bebida.</p>
<p>Mais que sabor, nosso serviço de bartender para festa de 15 anos foca na apresentação e no visual dos drinks. As técnicas de preparação das bebidas são um verdadeiro show, o que torna a experiência ainda mais divertida para todos. Outro diferencial é a montagem do bar conforme a temática da festa. </p>
<p>Com o conhecimento na preparação dos coquetéis e experiência nesse em diversos eventos, nosso serviço de bartender para festa de 15 anos proporciona agilidade e organização em cada processo. </p>
<p>Queremos fazer parte desse momento especial levando o melhor atendimento até você. Estamos cientes da importância da festa de debutante e da responsabilidade em servir com maestria a todos que chegam até nós. Entre em contato conosco para mais informações ou para solicitar um orçamento.</p>

                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>