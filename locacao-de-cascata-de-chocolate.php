<?php
    $title       = "Locação de Cascata de Chocolate";
    $description = "Na locação de cascata de chocolate da Vipdrinks, há diversas exclusividades para você garantir em seu evento. Navegue em nosso site para receber mais detalhes.";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    
<p>A Vipdrinks possui um vasto conhecimento em locação de cascata de chocolate, pois possuímos longos anos de experiência fornecendo esse e demais serviços para diversos eventos e festas. Conhecimentos que foram aplicados em práticas, para que nos tornemos cada vez mais referência com a nossa locação de cascata de chocolate. Fazemos a questão de priorizarmos os pedidos e as necessidades de nossos clientes, para que sejamos exemplos cem qualidade. Sabemos da importância que é para os nossos clientes, fazermos o que for pedido, da forma como nos foi solicitado. Portanto, nossos representantes absorvem o máximo de suas ideias, para que possamos colocar em prática tudo o que você imagina perante a nossa locação de cascata de chocolate. Todos os nossos profissionais possuem grandes experiências com locação de cascata de chocolate, portanto, os mesmos são extremante aptos para atenderem a qualquer tipo de pedido, até mesmo porque nós utilizamos dos melhores materiais e alimentos em nossa cascata, para que nos adaptemos a qualquer situação. Não perca essa oportunidade de poder usufruir de nossos serviços para qualquer evento que desejar. Nosso maior objetivo é realizarmos sonhos e fazer com que o seu dia seja ainda mais especial, com os nossos trabalhos. Utilizamos sempre técnicas exclusivas, que são sempre inovadas, para que sejamos únicos em qualidade nesse mercado. Desde nosso primeiro atendimento, nós fazemos um atendimento personalizado a você, com extrema excelência, para que sempre que você necessitar de serviços para qualquer tipo de evento, a Vipdrinks esteja dentro de suas opções.</p>
<h2></h2>
<h2>Conheça mais sobre nossa locação de cascata de chocolate</h2>
<p>Em todos os nossos anos fazendo a locação de cascata de chocolate, percebemos e vimos o quanto é um diferencial em seu evento. Portanto, queremos que todos tenham essa oportunidade de poder adquiri-la. Todavia, deixamos nossos valores maleáveis e acessíveis, para que a qualquer momento em que desejaram obter esse e demais serviços, os mesmos possam usufruir de nossa qualidade. Navegue em nosso site e faça o seu orçamento personalizado de forma rápida e sem sair de casa. Ou se preferir, consulte nossos representantes através de nossos meios de contatos.</p>

<h3>A melhor opção para locação de cascata de chocolate</h3>
<p>Você pode entrar em contato com nossos representantes a qualquer momento que desejar, para tirar suas dúvidas perante a nossa locação de cascata de chocolate; portanto, não adie mais essa oportunidade. Será um prazer fazermos parte da realização de um sonho seu.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>