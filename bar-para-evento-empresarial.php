<?php
    $title       = "Bar para Evento Empresarial";
    $description = "Todos os nossos meios de contatos disponibilizados em nosso site, estão sempre disponíveis para que você possa falar diretamente com um de nossos representantes.

";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Um evento empresarial é marcante na vida de muitas pessoas, pela amplitude de novos conhecimentos que acaba fornecendo. Imagina que incrível que será quando os seus convidados lembrarem de nosso bar para evento empresarial, consequentemente lembrar de todo conteúdo passado em seu evento?! Temos o objetivo de fazer com que a experiência de cada pessoa, em nosso bar para evento empresarial, seja memorável. Desde o cliente que entra em contato diretamente conosco, até os que estar presente no evento, para experimentar de nosso bar para evento empresarial. Todos os nossos profissionais possuem longas experiências não só na execução de bares para evento empresarial, mas para diversos tipos de eventos como: confraternização de empresas, aniversários, casamento… Portanto, nossos serviços de qualidades estão disponíveis para todos aqueles que nos procuram. Todavia, não hesite na hora de entrar em contato conosco, pois com a Vipdrinks você terá o todo o atendimento necessário, com os nossos profissionais sempre correspondendo as suas necessidades. Seja para fazer um orçamento sem compromisso perante ao nosso bar para evento empresarial, ou até mesmo para realizarmos nossas atividades dentro do seu evento, nós depositaremos toda a nossa atenção a você, para que possamos corresponder a todas as suas necessidades e pedidos.</p>

<h2>Mais detalhes sobre nosso bar para evento empresarial</h2>
<p>Até a data do seu evento, nós revisamos cada drink que será posto para que tudo saia da forma que imaginou, pois mantemos sempre suas ideias como nossa prioridade. Em nosso site você poderá fazer o seu orçamento conosco de forma online e rápida. Lembrando que nós mantemos os preços de nossos serviços extremamente acessíveis, para que a cada dia, mais pessoas tenham acesso a experiência incrível que é consultar nosso bar para evento empresarial. E caso prefira, todos os nossos meios de contatos estão sempre disponíveis para que você possa falar diretamente com um de nossos representantes.</p>

<h2>A melhor opção para bar para evento empresarial</h2>
<p>A cada evento, nós entregamos nossos melhores trabalhos para que possamos não só nos tornamos uma referência nesse ramo, mas uma empresa referência para você. Para que quando necessitar de um bar para evento empresarial, você recorra à Vipdrinks. Não perca mais tempo para garantir esse ou demais serviços que nós disponibilizamos em nossa empresa, para o seu determinado evento. Nossos profissionais são extremamente qualificados para te entregarem um serviço de alta qualidade e mantendo o seu baixo custo. A Vipdrinks está sempre disponível.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>