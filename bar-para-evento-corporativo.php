<?php
    $title       = "Bar para Evento Corporativo";
    $description = "Nossos meios de contatos estão sempre disponíveis para que você possa falar com um de nossos profissionais e até mesmo realizar o seu orçamento sem compromisso, ou não. ";
    $h1          = $title;
    $keywords    = $title;
    $meta_img    = "";

    include "includes/padrao/class.padrao.php";
    include "includes/config.php";
    include "includes/padrao/head.padrao.php";

    $url_title   = $padrao->formatStringToURL($title);

    $padrao->compressCSS(array(
        "tools/fancybox",
        "default_padrao/redes-sociais",
        "default_padrao/direitos-texto",
        "default_padrao/regioes",
        "default_padrao/veja-tambem",
        "palavra-chave"
    ));

?>
</head>
<body>

    <?php include "includes/_header.php"; ?>
    

    <main class="main-content">
        <section class="container">
            <?php echo $padrao->breadcrumb(array("Informações", $title)); ?>
            <h1 class="main-title"><?php echo $h1; ?></h1>
            <div class="row">
                <div class="col-md-9 text-justify">
                    <?php echo $padrao->listaGaleria($h1, 4); ?>
                    <a href="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" title="<?php echo $h1; ?>" class="img-fancy-pc">
                        <img src="<?php echo $url."imagens/thumbs/".$url_title.".jpg"; ?>" alt="<?php echo $h1; ?>" title="<?php echo $h1; ?>" class="img-right img-responsive">
                    </a>
                    <p>Sabemos da importância que há em um evento corporativo, portanto, possuir um bar para evento corporativo é extremamente necessário, para que o mesmo tenha mais requinte e sofisticação. Levando mais conforto a quem está no mesmo. Seja para eventos com grande proporção, ou não, nosso bar para evento corporativo é totalmente apto. E independente de como ou qual for, nós atendemos sempre as expectativas daqueles que nos procuram e dos que estão presentes no evento também. Pois todos os nossos anos atuando como bar para evento corporativo e em demais outros, nos mostraram isso. Além de prestarmos serviços para eventos corporativos, nosso bar está disponível para eventos como: confraternização de empresas, casamentos, aniversários, entre outros. Portanto, garantimos que você pode recorrer aos nossos serviços para qualquer evento que desejar. Todos os nossos profissionais possuem longas experiências, pois atuam em nosso bar para evento corporativo há um tempo. Contudo, afirmamos que independente de sua solicitação perante aos nossos serviços, nós conseguiremos lhe atender por toda experiência que possuímos. Um de nossos princípios está em prestarmos sempre nosso melhor serviço, seja para o que formos solicitados. Tanto para que você recorra ao nosso bar para eventos corporativos quando precisar, quanto para te fornecer uma ótima experiência como cliente. Com a Vipdrinks, todas as suas necessidades serão atendidas e correspondidas.</p>

<h2>Mais detalhes sobre o nosso bar para evento corporativo</h2>
<p><br />Não hesitamos ao oferecer nosso excelente trabalho de bar para evento corporativo, pois todos os nossos anos de atuação nesse meio, nos mostraram a qualidade de nossos produtos, através da satisfação de nossos clientes. Para que você tenha os melhores drinks e de sua escolha, em seu evento, tudo o que será feito no mesmo, é revisado com você, para que nada passe despercebido. Garantimos que nossos valores são acessíveis, pois buscamos levar a nossa qualidade a quem nos buscar, portanto, não se preocupe quando for nos procurar pois temos as melhores condições a você.</p>
<h2><br />A melhor opção para bar para evento corporativo</h2>
<p><br />Nossos meios de contatos estão sempre disponíveis para que você possa falar com um de nossos profissionais e até mesmo realizar o seu orçamento perante ao nosso bar para evento corporativo. Caso prefira, em nosso site há uma parte disponível para que você possa fazer o mesmo de forma rápida e online. Possuímos diversas redes sociais para que você possa acompanhar nossos trabalhos e obter informações mais detalhadas também. Conte conosco.</p>
                    <?php include "includes/social-media.php"; ?>
                    <?php include "includes/regioes-sao-paulo.php"; ?>
                    <?php // include "includes/regioes-brasil.php"; ?>
                    <?php include "includes/veja-tambem.php"; ?>
                    <?php include "includes/direitos-texto.php"; ?>
                </div>
                <aside class="col-md-3">
                    <?php include "includes/sidebar.php"; ?>
                </aside>
            </div>
        </section>
    </main>

    <?php include "includes/_footer.php"; ?>

    <?php $padrao->compressJS(array(
        "tools/jquery.fancybox",
        "tools/bootstrap.min",
        "tools/jquery.validate.min",
        "tools/jquery.mask.min",
        "jquery.quality.keyword"
    )); ?>

</body>
</html>